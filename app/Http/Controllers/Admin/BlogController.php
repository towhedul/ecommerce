<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Response;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
      $blogs=DB::table('tbl_blog')
            ->where('publication_status',1)
            ->get();
        return view('admin.blog.index',compact('blogs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.blog.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //$data=$request->all();

        $this->validate($request,[

            'title'=>'required',
            'date'=>'required',
            'time'=>'required',
            'short_description'=>'required',
            'long_description'=>'required',
            'publication_status'=>'required',
            'blog_image'=>'required | mimes:jpeg,png,jpg,gif,svg'

        ]);

        $data=array();

        $data['title']=$request->title;

        $data['date'] = date('Y-m-d', strtotime($request->date));


        //$data['date']=$request->date;
        $data['time']=$request->time;
        //$data['product_image']=$request->product_image;
        $data['short_description']=$request->short_description;
        $data['long_description']=$request->long_description;
        $data['publication_status']=$request->publication_status;

        $image=$request->file('blog_image');
        if (isset($image)) {
            
            $image_name=$image->getClientOriginalName();
            $image_ext=strtolower($image->getClientOriginalExtension());
            $image_full_name=$image_name.'-'.uniqid().'.'.$image_ext;

            if (!file_exists('upload/blog')) {
                
                mkdir('upload/blog',0777,true);
            }

            $success=$image->move('upload/blog',$image_full_name);
            if ($success) {
               $data['blog_image']=$image_full_name;             
            }
                          
          } 

       DB::table('tbl_blog')->insert($data);
      
      return redirect()->route('blog.index')->with('successMsg','Data is Insert Successfully !!');    
       

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
      $blog=DB::table('tbl_blog')
           ->where('id',$id)
           ->first();  
        return view('admin.blog.edit',compact('blog'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data=array();

        $data['title']=$request->title;

        $data['date'] = date('Y-m-d', strtotime($request->date));


        //$data['date']=$request->date;
        $data['time']=$request->time;
        //$data['product_image']=$request->product_image;
        $data['short_description']=$request->short_description;
        $data['long_description']=$request->long_description;
        
        $blog=DB::table('tbl_blog')
             ->where('id',$id)
             ->first();


        $image=$request->file('blog_image');
        if (isset($image)) {
            
            $image_name=$image->getClientOriginalName();
            $image_ext=strtolower($image->getClientOriginalExtension());
            $image_full_name=$image_name.'-'.uniqid().'.'.$image_ext;

            if (!file_exists('upload/blog')) {
                
                mkdir('upload/blog',0777,true);
            }

            $success=$image->move('upload/blog',$image_full_name);
            if ($success) {
               $data['blog_image']=$image_full_name;             
            }else{

                $data['blog_image']=$blog->blog_image;
            }
                          
          } 

       DB::table('tbl_blog')->where('id',$id)->update($data);
      
      return redirect()->route('blog.index')->with('successMsg','Data is Updated Successfully !!');    
       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $blog=DB::table('tbl_blog')
               ->where('id',$id)
               ->first();
             

       if (file_exists('upload/blog/'.$blog->blog_image)) {
           unlink('upload/blog/'.$blog->blog_image);           
       }
       DB::table('tbl_blog')->where('id',$id)->delete();

       return redirect()->route('blog.index')->with('successMsg','Data is deleted successfully !!');
    }
}
