<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class BrandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $brands=DB::table('tbl_brand')->get();

        return view ('admin.brand.index',compact('brands'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.brand.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[

             'brand_name'=>'required',
             'brand_description'=>'required',
             'publication_status'=>'required',        
        ]);

        $data=array();

        //$data['brand_id']=$request->brand_id;
        $data['brand_name']=$request->brand_name;
        $data['brand_description']=$request->brand_description;
        $data['publication_status']=$request->publication_status;

        DB::table('tbl_brand')->insert($data);
        return redirect()->route('brand.index')->with('successMsg','Data Inserted Successfully');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($brand_id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($brand_id)
    {
        $brands=DB::table('tbl_brand')
                   ->where('brand_id',$brand_id)
                   ->first();
      return view('admin.brand.edit',compact('brands'));             
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $brand_id)
    {
       
        $this->validate($request,[
            
            'brand_name'=>'required',
            'brand_description'=>'required',

        ]);

        $data=array();

        $data['brand_name']=$request->brand_name;
        $data['brand_description']=$request->brand_description;
        

        DB::table('tbl_brand')
                ->where('brand_id',$brand_id)
                ->update($data);

        return redirect()->route('brand.index')->with('successMsg','Data Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($brand_id)
    {
        DB::table('tbl_brand')
               ->where('brand_id',$brand_id)
               ->delete();
        return redirect()->back()->with('successMsg','Data Deleted Successfully');
    }
}
