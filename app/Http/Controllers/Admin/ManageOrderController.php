<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class ManageOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()

    {   
        $orders = DB::table('tbl_orders')
                ->join('tbl_customers','tbl_orders.customer_id','=','tbl_customers.customer_id')
                ->join('tbl_payments','tbl_orders.payment_id','=','tbl_payments.payment_id')
                ->get();
        return view ('admin.manage_order',compact('orders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($order_id)
    {
        /*echo $order_id;*/
        
        /*return view('admin.order_details');*/
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($order_id)
    {
        echo $order_id;
        $orders = DB::table('tbl_orders')
                ->where('order_id',$order_id)
                ->first();

        return view('admin.change_order_status',compact('orders'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $order_id)
    {
        
        $data = array();
        $data['status']=$request->status;


       DB::table('tbl_orders')
            ->where('order_id',$order_id)
            ->update($data);

        return redirect('admin/manage_order');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($order_id)
    {
        DB::table('tbl_orders')
              ->where('order_id',$order_id)
              ->delete();

       return redirect()->back()->with('successMsg','Record is deleted successfully');
    }
}
