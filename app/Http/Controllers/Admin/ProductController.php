<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;


class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products=DB::table('tbl_products')
                 ->join('tbl_category','tbl_products.category_id','=','tbl_category.category_id')
                 ->join('tbl_sub_category','tbl_products.sub_category_id','=','tbl_sub_category.sub_category_id')
                 
                 ->select('tbl_products.*','tbl_category.category_name','tbl_sub_category.sub_category_name')
                 ->get();
        return view('admin.products.index',compact('products'));

                 //print_r($products);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.products.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $this->validate($request,[

              'product_name' => 'required',
              'category_id' => 'required',
              'sub_category_id' => 'required',
              'product_short_description' => 'required',
              'product_long_description' => 'required',
              'product_price' => 'required',
              'product_size' => 'required',
              'product_color' => 'required',
              'product_quantity' => 'required',
              'publication_status' => 'required',
              'product_image' => 'required|image|mimes:jpg,jpeg,png,gif',
        ]);

        $data = array();
       
        $data['product_name'] = $request->product_name;
        $data['category_id'] = $request->category_id;
        $data['sub_category_id'] = $request->sub_category_id;
        $data['product_short_description'] = $request->product_short_description;
        $data['product_long_description'] = $request->product_long_description;
        $data['brand_name'] = $request->brand_name;
        $data['product_price'] = $request->product_price;
        $data['product_size'] = $request->product_size;
        $data['product_color'] = $request->product_color;
        $data['product_quantity'] = $request->product_quantity;
        $data['publication_status'] = $request->publication_status;

        $image=$request->file('product_image');

    if ($image) {

        $image_name = $image->getClientOriginalName();
        $ext=strtolower($image->getClientOriginalExtension());
        $image_full_name=$image_name.uniqid().'.'.$ext;
        $image_path='image/';
        $image_url=$image_path.$image_full_name;
        $success=$image->move($image_path,$image_full_name);
        if ($success) {
            $data['product_image']=$image_url;

         DB::table('tbl_products')->insert($data);
         return redirect()->route('products.index')->with('successMsg','Data Added Successfully!!');

        }         
     } 
        

        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($product_id)
    {
        $products = DB::table('tbl_products')
                  ->join('tbl_category','tbl_products.category_id','=','tbl_category.category_id')
                  ->join('tbl_sub_category','tbl_products.sub_category_id','=','tbl_sub_category.sub_category_id')

                  ->where('product_id',$product_id)
                  ->first();
        return view ('admin.products.edit',compact('products'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $product_id)
    {
        $this->validate($request,[

              'product_name' => 'required',
              'category_id' => 'required',
              'sub_category_id' => 'required',
              'product_short_description' => 'required',
              'product_long_description' => 'required',
              'product_price' => 'required',
              'product_size' => 'required',
              'product_color' => 'required',
              'product_quantity' => 'required',
              'product_image' => 'sometimes|image|mimes:jpg,jpeg,png,gif',
        ]);

        $data=array();

        $data['product_name']=$request->product_name;
        $data['category_id']=$request->category_id;
        $data['sub_category_id']=$request->sub_category_id;
        $data['product_short_description']=$request->product_short_description;
        $data['product_long_description']=$request->product_long_description;
        $data['brand_name'] = $request->brand_name;
        $data['product_price']=$request->product_price;
        $data['product_size']=$request->product_size;
        $data['product_color']=$request->product_color;
        $data['product_quantity']=$request->product_quantity;

        
    
        $image=$request->file('product_image');

      if(isset($image)){

         $image_name=$image->getClientOriginalName();
         $ext=strtolower( $image->getClientOriginalExtension());
         $image_ful_name=$image_name.uniqid().'.'.$ext;
         $image_path='image/';
         $image_url=$image_path.$image_ful_name;
         $success=$image->move($image_path,$image_ful_name);
        if($success){
            $data['product_image']=$image_url;
            
         }

      } 
      DB::table('tbl_products')
                 ->where('product_id',$product_id)
                 ->update($data);
                 return redirect()->route('products.index')->with('successMsg','Data Updated Successfully!!'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($product_id)
    {
        $product=DB::table('tbl_products')
                ->where('product_id',$product_id)
                ->first();
        if(file_exists($product->product_image)){

            unlink($product->product_image);

        }
        
        $product->delete();
        return redirect()->back()->with('successMsg','Data Deleted Successfully!!');

    }
}
