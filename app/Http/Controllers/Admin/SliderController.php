<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sliders=DB::table('tbl_sliders')->get();
        return view('admin.slider.index',compact('sliders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('admin.slider.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
       $this->validate($request,[

          'image_title'=>'required',
          'image_subtitle'=>'required',
          'publication_status'=>'required',
          'slider_image'=>'required | mimes:jpeg,png,jpg,gif,svg'

       ]);

        $data=array();
        $data['image_title']=$request->image_title;
        $data['image_subtitle']=$request->image_subtitle;
        $data['publication_status']=$request->publication_status;
        
        
        $image=$request->file('slider_image');
        if (isset($image)) {
            
            $image_name=$image->getClientOriginalName();
            $image_ext=strtolower($image->getClientOriginalExtension());
            $image_full_name=$image_name.'-'.uniqid().'.'.$image_ext;

            if (!file_exists('upload/slider')) {
                
                mkdir('upload/slider',0777,true);
            }

            $success=$image->move('upload/slider',$image_full_name);
            if ($success) {
               $data['slider_image']=$image_full_name;
             
            }
                          
          } 
          

        DB::table('tbl_sliders')->insert($data);
      
      return redirect()->route('slider.index')->with('successMsg','Data Added Successfully !!');  

      //print_r($data);         
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $slider=DB::table('tbl_sliders')
               ->where('id',$id)
               ->first();
        return view('admin.slider.edit',compact('slider'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data=array();

        $data['image_title']=$request->image_title;
        $data['image_subtitle']=$request->image_subtitle;
        
        $slider=DB::table('tbl_sliders')
               ->where('id',$id)
               ->first();

        $image=$request->file('slider_image');

        if (isset($image)) {
            $image_name=$image->getClientOriginalName();
            $img_ext=strtolower($image->getClientOriginalExtension());
            $image_full_name=$image_name.'-'.uniqid().'.'.$img_ext;

            if (!file_exists('upload/slider')) {
                
                 mkdir('upload/slider',0777,true);
            }
            $success=$image->move('upload/slider',$image_full_name);
            if ($success) {
                $data['slider_image']=$image_full_name;
                //unlink($slider->slider_image);
            }else{
                $data['slider_image']=$slider->slider_image;
            }
        }

        DB::table('tbl_sliders')->where('id',$id)->update($data);

        return redirect()->route('slider.index')
                     ->with('successMsg','slider is updated successfullt !!');
       

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $slider=DB::table('tbl_sliders')
               ->where('id',$id)
               ->first();
             

       if (file_exists('upload/slider/'.$slider->slider_image)) {
           unlink('upload/slider/'.$slider->slider_image);           
       }
       DB::table('tbl_sliders')->where('id',$id)->delete();

       return redirect()->route('slider.index')->with('successMsg','Slider is deleted successfully !!');
       
    }
}
