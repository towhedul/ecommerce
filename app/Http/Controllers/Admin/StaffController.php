<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class StaffController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $staffs=DB::table('tbl_staffs')
               ->get();

        return view('admin.staff.index',compact('staffs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.staff.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[

         'name'=>'required',
         'email'=>'required',
         'address'=>'required',
         'area'=>'required',
         'mobile_number'=>'required',
         'image'=>'required |image| mimes:jpeg,png,jpg,gif,svg'

         ]);

        $data=$request->all();

        
        $data=array();
        $data['name']=$request->name;
        $data['email']=$request->email;
        $data['address']=$request->address;
        $data['area']=$request->area;
        $data['mobile_number']=$request->mobile_number;
        $data['publication_status']=$request->publication_status;

        $image=$request->file('image');
        if (isset($image)) {
            
            $image_name=$image->getClientOriginalName();
            $image_ext=strtolower($image->getClientOriginalExtension());
            $image_full_name=$image_name.'-'.uniqid().'.'.$image_ext;

            if (!file_exists('upload/staff')) {
                
                mkdir('upload/staff',0777,true);
            }

            $success=$image->move('upload/staff',$image_full_name);
            if ($success) {
               $data['image']=$image_full_name;
             
            }
                          
          }
          
    DB::table('tbl_staffs')->insert($data);
return redirect()->route('staff.index')->with('successMsg','Data is Updated Successfully !!');

        /*print_r($data);*/
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $staff=DB::table('tbl_staffs')
               ->where('id',$id)
               ->first();
        return view('admin.staff.edit',compact('staff'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[

         'name'=>'required',
         'email'=>'required',
         'address'=>'required',
         'area'=>'required',
         'mobile_number'=>'required',
         'image'=>'required|sometimes|image|mimes:jpeg,png,jpg,gif,svg'

         ]);


        $data=array();
        $data['name']=$request->name;
        $data['email']=$request->email;
        $data['address']=$request->address;
        $data['area']=$request->area;
        $data['mobile_number']=$request->mobile_number;
        /*$data['publication_status']=$request->publication_status;*/

        $staff=DB::table('tbl_staffs')
               ->where('id',$id)
               ->first();

        $image=$request->file('image');
        if (isset($image)) {
            
            $image_name=$image->getClientOriginalName();
            $image_ext=strtolower($image->getClientOriginalExtension());
            $image_full_name=$image_name.'-'.uniqid().'.'.$image_ext;

            if (!file_exists('upload/staff')) {
                
                mkdir('upload/staff',0777,true);
            }

            $success=$image->move('upload/staff',$image_full_name);
            if($success) {
               $data['image']=$image_full_name;
             
            }else{
                $data['image']=$staff->image;
            }
                          
          }
          
    DB::table('tbl_staffs')->update($data);
return redirect()->route('staff.index')->with('successMsg','Data is Edited Successfully !!');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $staff=DB::table('tbl_staffs')
               ->where('id',$id)
               ->first();
             

       if(file_exists('upload/staff/'.$staff->image)) {
           unlink('upload/staff/'.$staff->image);           
       }
       DB::table('tbl_staffs')->where('id',$id)->delete();

       return redirect()->route('staff.index')->with('successMsg','Staff is deleted successfully !!');
    }
}
