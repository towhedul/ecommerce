<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use DB;
use App\Http\Controllers\Controller;

class SubCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()

    {
        
        $sub_categories=DB::table('tbl_sub_category')
                     ->join('tbl_category','tbl_sub_category.category_id','=','tbl_category.category_id')
                     ->select('tbl_sub_category.*','tbl_category.category_name')
                     
                     ->get();

       return view ('admin.subCategory.index',compact('sub_categories'));

        //print_r($sub_categories);             
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = DB::table('tbl_category')->get();
        return view ('admin.subCategory.create',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[

             'sub_category_name' => 'required',
             'sub_category_description' => 'required',
             'publication_status' => 'required',
        ]);

        $data=array();

        $data['sub_category_id']=$request->sub_category_id;
        $data['category_id']=$request->category_id;
        $data['sub_category_name']=$request->sub_category_name;
        $data['sub_category_description']=$request->sub_category_description;
        $data['publication_status']=$request->publication_status;
 
    DB::table('tbl_sub_category')->insert($data);
    return redirect()->route('subCategory.index')->with('successMsg','Data Added Successfully !');
     
    }

    
    



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($sub_category_id)
    {
       $sub_categories=DB::table('tbl_sub_category')
                      ->join('tbl_category','tbl_sub_category.category_id','=','tbl_category.category_id')
                      ->where('sub_category_id',$sub_category_id)
                      ->first();    
    
        return view('admin.subCategory.edit',compact('sub_categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $sub_category_id)
    {
        $this->validate($request,[

             'sub_category_name' => 'required',
             'sub_category_description' => 'required',
        ]);

        $data = array();

        $data['category_id']=$request->category_id;
        $data['sub_category_name']=$request->sub_category_name;
        $data['sub_category_description']=$request->sub_category_description;

            DB::table('tbl_sub_category')
                    ->where('sub_category_id',$sub_category_id)
                    ->update($data);
             
             return redirect()->route('subCategory.index')->wuth('successMsg','Data Updated Successfully!!');
    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($sub_category_id)
    {
        DB::table('tbl_sub_category')
               ->where('sub_category_id',$sub_category_id)
               ->delete();
        return redirect()->back()->with('successMsg','Data Deleated Successfully!!');
    }
}





