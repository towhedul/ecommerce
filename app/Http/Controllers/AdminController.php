<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use DB;
use Session;
use PDF;

session_start();

class AdminController extends Controller
{
    public function index(){

    	return view('admin.login');
    }

    

    public function dashboard(Request $request){

   	    $admin_email = $request->admin_email;
   	    $admin_password = ($request->admin_password);

   	 $result=DB::table('tbl_admin') 
   	          ->where('admin_email',$admin_email)
   	          ->where('admin_password',$admin_password)
   	          ->first();

   	    if ($result) {
   	          	
   	          	Session::put('admin_name',$result->admin_name);
   	          	Session::put('admin_id',$result->admin_id);

   	          	return Redirect::to('/dashboard');
   	          } 
   	          else{

                  Session::put('message','Email or Password Invailed');
                  return redirect()->back();
   	          }    

   }

   public function show_order_details($order_id){
      
      $order_details = DB::table('tbl_order_details')
                     ->join('tbl_orders','tbl_order_details.order_id','=','tbl_orders.order_id')
                     ->join('tbl_shippings','tbl_order_details.shipping_id','=','tbl_shippings.shipping_id')
                     ->join('tbl_customers','tbl_order_details.customer_id','=','tbl_customers.customer_id')
                     ->select('tbl_order_details.*','tbl_orders.order_id','tbl_orders.order_date','tbl_orders.total_amount','tbl_shippings.*','tbl_customers.*')
                     ->where('tbl_order_details.order_id',$order_id)
                     ->get();
      return view ('admin.show_order_details',compact('order_details'));
    }

    public function invoice($order_id){

      $order = DB::table('tbl_order_details')
               ->where('tbl_order_details.order_id',$order_id)
               ->get();
       

      $pdf = PDF::loadView('admin.invoice',compact('order'));
      return $pdf->stream('invoice.pdf');
      //return view('admin.invoice');
    }
    
}
