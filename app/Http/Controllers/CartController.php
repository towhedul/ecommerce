<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Session;
use DB;
use Cart;

class CartController extends Controller
{
    public function add_to_cart(Request $request){

    	$qty=$request->quantity;
    	$product_id=$request->product_id;

    	$product_info=DB::table('tbl_products')
    	              ->where('product_id',$product_id)
    	              ->first();
        
        $data['quantity']=$qty;
        $data['id']=$product_info->product_id;
        $data['name']=$product_info->product_name;
        $data['price']=$product_info->product_price;
        $data['attributes']['image']=$product_info->product_image;

        Cart::add($data);


        return Redirect::to('/show_cart'); 
       
    }

    public function show_cart(){

    	return view('Pages.add_to_cart');
    }

    public function delete_to_cart($id){

    	  Cart::remove($id);
    	  return redirect()->back();
    }

    public function increment_quantity($id){

        
         Cart::update($id,[
        'quantity' => 1
    ]);
         return redirect()->back();

    }

    public function decrement_quantity($id){

        
         Cart::update($id,[
        'quantity' => -1
    ]);
         return redirect()->back();

    }


    public function show_orderDetails_id($order_id){

        $order_details = DB::table('tbl_order_details')
                       ->join('tbl_customers','tbl_order_details.customer_id','=','tbl_customers.customer_id')

                       ->select('tbl_order_details.*','tbl_customers.customer_name')
                       ->where('tbl_order_details.order_id',$order_id)
                       ->get();

        return view('admin.show',compact('order_details'));               
    }




}
