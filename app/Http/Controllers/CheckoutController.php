<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Session;
use Cart;
use DB;

class CheckoutController extends Controller
{
    public function login_check(){

    	return view ('Pages.user_login');
    }

    public function user_registration(Request $request){

          //$data=$request->all();
        $this->validate($request,[

            'customer_name'=>'required',
            'customer_email'=>'required | email',
            'Password'=>'required',
            'mobile_number'=>'required',
            'address'=>'required',
            'city'=>'required',
            'post_code'=>'required',

        ]);

        $data=array();

        $data['customer_name']=$request->customer_name;
        $data['customer_email']=$request->customer_email;
        $data['Password']=md5($request->Password);
        $data['mobile_number']=$request->mobile_number;
        $data['address']=$request->address;
        $data['city']=$request->city;
        $data['post_code']=$request->post_code;

        $customer_id=DB::table('tbl_customers')
                    ->insertGetId($data);

        Session::put('customer_id',$customer_id);
        Session::put('customer_name',$request->customer_name);

        return redirect('/checkout');



          /*print_r($data);*/
    }

    public function checkout(){

    	return view('Pages.checkout');
    }

    public function shipping_details(Request $request){
            
        /*$data=$request->all();*/

        $this->validate($request,[

         'shipping_email'         =>'required',
         'shipping_first_name'    =>'required',
         'shipping_last_name'     =>'required',
         'shipping_address'       =>'required',
         'shipping_mobile_number' =>'required',
         'shipping_city'          =>'required'

        ]);

        $data=array();

        $data['shipping_email']=$request->shipping_email;
        $data['shipping_first_name']=$request->shipping_first_name;
        $data['shipping_last_name']=$request->shipping_last_name;
        $data['shipping_address']=$request->shipping_address;
        $data['shipping_mobile_number']=$request->shipping_mobile_number;
        $data['shipping_city']=$request->shipping_city;

        $shipping_id=DB::table('tbl_shippings')
                    ->insertGetId($data);

        Session::put('shipping_id',$shipping_id);
        return Redirect('/payment');

    }

    public function payment(){

        return view('Pages.payment');
    }

    public function payment_success(){

        return view ('Pages.payment_success');
    }

    public function success_done(){

        return view('Pages.success_done');       

    }

    public function store(Request $request){

       
       try {

         // Set your secret key: remember to change this to your live secret key in production
        // See your keys here: https://dashboard.stripe.com/account/apikeys
        \Stripe\Stripe::setApiKey('sk_test_ubpjYpwN3JEgSd8xxUqWoOJt00LwOrvWiX');

        // Token is created using Checkout or Elements!
        // Get the payment token ID submitted by the form:
        $token = $_POST['stripeToken'];
        $charge = \Stripe\Charge::create([
            'amount' => Cart::getTotal() * 100,
            'currency' => 'USD',
            'description' => 'Example charge',
            'source' => $token,
        ]);

       /*echo "<pre>" ;
       print_r($charge);
       echo "<pre>" ;*/

       if ($charge) {

           $data=array();
           $data['transaction_id'] = $charge['id'];
           $data['payment_method'] = $charge['payment_method'];
           $data['payment_type'] = strtoupper($charge['payment_method_details']['card']['brand']);
           $data['card_number'] = $charge['payment_method_details']['card']['last4'];
           $data['currency'] =strtoupper($charge['currency']);
           $data['amount'] = $charge['amount'];
           $data['payment_status'] = $charge['status'];
           $data['receipt_email'] = $charge['receipt_email'];
           $data['receipt_url'] = $charge['receipt_url'];
           $data['postal_code'] = $charge['billing_details']['address']['postal_code'];

           $payment_id=DB::table('tbl_payments')
                   ->insertGetId($data);

           Session::put('payment_id',$payment_id); 



          $data=array();
          date_default_timezone_set("Asia/Dhaka");
          $data['customer_id'] = Session::get('customer_id');
          $data['payment_id'] = Session::get('payment_id');
          $data['quantity'] = Cart::getTotalQuantity();
          $data['total_amount'] = $charge['amount']/100;
          $data['order_date'] = date('d/m/Y || h:i:s A', time());
          $data['status'] = 0;

          $order_id=DB::table('tbl_orders')
                   ->insertGetId($data);

           Session::put('order_id',$order_id); 


         $products = Cart::getContent();
         $data=array();
       foreach($products as $product){

         $data['order_id'] = Session::get('order_id');
         $data['customer_id'] = Session::get('customer_id');
         $data['shipping_id'] = Session::get('shipping_id');
         $data['product_id'] = $product->id;
         $data['product_name'] = $product->name;
         $data['product_image'] = $product->attributes->image;
         $data['quantity'] = $product->quantity;
         $data['price'] = $product->price;

         DB::table('tbl_order_details')->insert($data);
         
         } 

         Cart::clear();  // For clearing the card         
       }     
       } catch (Exception $e) {
           echo "Something Wrong";
       }

       return Redirect('/success_done');

    }


    public function user_login(Request $request){

       $customer_email=$request->customer_email;
       $password=md5($request->password);

     $result=DB::table('tbl_customers')
            ->where('customer_email',$customer_email)
            ->where('password',$password)
            ->first();

        if ($result) {
        	   Session::put('customer_id',$result->customer_id);
        	   Session::put('customer_name',$result->customer_name);
        	 return redirect('/checkout');
        }else{

        	return redirect()->back();
        }
            
    }

    public function user_logout(){

    	Session::flush();
    	return Redirect('/');
    }
        


}






