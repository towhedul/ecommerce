<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ExtraPagesController extends Controller
{
    public function show_contact(){

    	return view('Pages.contact');
    }

    public function blog_show(){

    	return view('Pages.blog');
    }
}
