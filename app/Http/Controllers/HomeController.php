<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;

class HomeController extends Controller
{
    public function index(){

    	$all_published_products=DB::table('tbl_products')
                 ->join('tbl_category','tbl_products.category_id','=','tbl_category.category_id')
                 ->join('tbl_sub_category','tbl_products.sub_category_id','=','tbl_sub_category.sub_category_id')
                 ->select('tbl_products.*','tbl_category.category_name','tbl_sub_category.sub_category_name')
                 ->where('tbl_products.publication_status',1)
                 ->limit(18)
                 ->get();

     
    	return view('Pages.home_content',compact('all_published_products'));
    }



    public function product_by_subcategory($sub_category_id){

             //echo $sub_category_id;
     $all_published_products_subcategories=DB::table('tbl_products')
                    ->join('tbl_sub_category','tbl_products.sub_category_id','=','tbl_sub_category.sub_category_id')
                    ->select('tbl_products.*','tbl_sub_category.sub_category_name')
                    ->where('tbl_sub_category.sub_category_id',$sub_category_id)
                    ->where('tbl_products.publication_status',1)
                    ->limit(9)
                    ->get();

      return view('Pages.product_by_subcategory',compact('all_published_products_subcategories'));

    }

    public function product_by_brand($brand_name){

        $all_published_products_brands=DB::table('tbl_products')
                          ->where('brand_name',$brand_name)
                          ->where('publication_status',1)
                          ->limit(18)
                          ->get();

        return view('Pages.product_by_brand_name',compact('all_published_products_brands'));                  
    }


    public function product_details($product_id){
 
        $published_products=DB::table('tbl_products')
                           ->where('product_id',$product_id)
                           ->where('publication_status',1)
                           ->first();
           return view('Pages.product_details',compact('published_products'));
    }
}
