<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Api\ExecutePayment;
use PayPal\Api\PaymentExecution;


use Cart;
use DB;
use Session;

class PaymentByPaypalController extends Controller
{
    public function index(){

    	return view('Pages.payment_by_paypal');
    }

    public function paypal(){

    	$apiContext = new \PayPal\Rest\ApiContext(
		  new \PayPal\Auth\OAuthTokenCredential(
		    env('PAYPAL_CLIENT_ID'),
		    env('PAYPAL_SECRET_ID')
		  )
		);

		// Create new payer and method
		$payer = new Payer();
		$payer->setPaymentMethod("paypal");

		// Set redirect URLs
		$redirectUrls = new RedirectUrls();
		$redirectUrls->setReturnUrl(route('process.paypal'))
		  ->setCancelUrl(route('cancel.paypal'));



		 
		

		// Set payment amount
		$amount = new Amount();
		$amount->setCurrency("USD")
		       
		       ->setTotal(Cart::getTotal());

         
        //Set Item object
		 /* 
		   $products = Cart::getContent();
        $data=array();
       foreach($products as $product){
			$product_name = $product->name;
         

         	$data=array($product_name);
        	
            
         }
          echo "<pre>";
			print_r($data);

		 die;*/

         
		 $item_1 = new Item();
         $item_1->setName('Item 1') /** item name **/
            ->setCurrency('USD')
            ->setQuantity(1)
            ->setPrice(Cart::getTotal());


        $item_list = new ItemList();
        $item_list->setItems(array($item_1));

/*
        $details = new Details(); 
        $details ->setShipping(10) 
                 -> setTax(90) 
                 -> setSubtotal(56);  */

        


		// Set transaction object
		
		$transaction = new Transaction();
		$transaction->setAmount($amount)
		          ->setItemList($item_list)
		          /*->setDetails($details)*/
		 		  ->setDescription('Description');

		// Create the full payment object
		$payment = new Payment();
		$payment->setIntent('sale')
		  ->setPayer($payer)
		  ->setRedirectUrls($redirectUrls)
		  ->setTransactions(array($transaction));

	  // Create payment with valid API context

		try {
		  $payment->create($apiContext);

		  // Get PayPal redirect URL and redirect the customer
		  $approvalUrl = $payment->getApprovalLink();
          if ($approvalUrl) {
          	
          	  $data=array();

          	  $data['customer_id']=Session::get('customer_id');
          	  $data['customer_name']=Session::get('customer_name');
          	  $data['shipping_id']=Session::get('shipping_id');
          }

		  return redirect($approvalUrl);

		  // Redirect the customer to $approvalUrl
		} catch (PayPal\Exception\PayPalConnectionException $ex) {
		  echo $ex->getCode();
		  echo $ex->getData();
		  die($ex);
		} catch (Exception $ex) {
		  die($ex);
		}



    }

    public function returnPaypal(Request $request){

	    	$apiContext = new \PayPal\Rest\ApiContext(
		  new \PayPal\Auth\OAuthTokenCredential(
		    env('PAYPAL_CLIENT_ID'),
		    env('PAYPAL_SECRET_ID')
		  )
		);
       
       // Get payment object by passing paymentId
			$paymentId = $request->paymentId;
			$payment = Payment::get($paymentId, $apiContext);
			$payerId = $request->PayerID;

			// Execute payment with payer ID
			$execution = new PaymentExecution();
			$execution->setPayerId($payerId);

			try {
			  // Execute payment
			  $result = $payment->execute($execution, $apiContext);
              dd($result);
			  if (isset($result)) {

			  $data=array();

          	  $data['transaction_id']=$result->id;
          	  $data['payment_type']=$result->payer->payment_method;
          	  $data['card_number']=$result->cart;
          	  $data['currency']=$result->transactions[0]->amount->currency;
          	  $data['amount']=Cart::getTotal();
          	  $data['state']=$result->state;
          	  $data['payment_status']=$result->payer->status;          	  
          	  $data['payer_id']=$result->payer->payer_info->payer_id;
          	  $data['receipt_url']=$result->links[0]->href;
          	  
          	  
			  }
			  /*dd($data);*/

			} catch (PayPal\Exception\PayPalConnectionException $ex) {
			  echo $ex->getCode();
			  echo $ex->getData();
			  die($ex);
			} catch (Exception $ex) {
			  die($ex);
			}

    }

    public function cancelPaypal(){


    }

}
