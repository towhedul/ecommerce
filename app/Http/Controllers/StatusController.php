<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class StatusController extends Controller
{
    public function unactive($category_id){

    	DB::table('tbl_category')
    	      ->where('category_id',$category_id)
    	      ->update(['publication_status'=>0]);

    	      return redirect()->back();

    }

    public  function active($category_id){

    	DB::table('tbl_category')
    	     ->where('category_id',$category_id)
    	     ->update(['publication_status'=>1]);

    	     return redirect()->back();
    }
    public function unactive_subcategory($sub_category_id){
        
        DB::table('tbl_sub_category')
                 ->where('sub_category_id',$sub_category_id)
                 ->update(['publication_status'=>0]);

          return redirect()->back();
    }

    public function active_subcategory($sub_category_id){

        DB::table('tbl_sub_category')
                ->where('sub_category_id',$sub_category_id)
                ->update(['publication_status'=>1]);
        return redirect()->back();
    }

    public function unactive_product($product_id){

        DB::table('tbl_products')
                 ->where('product_id',$product_id)
                 ->update(['publication_status'=>1]);
        return redirect()->back();
    }

    public function active_product($product_id){
        
        DB::table('tbl_products')
                 ->where('product_id',$product_id)
                 ->update(['publication_status'=>0]);
        return redirect()->back();
    }

    public function active_slider($id){

        DB::table('tbl_sliders')
                 ->where('id',$id)
                 ->update(['publication_status'=>0]);
          return redirect()->back();       
    }
    public function unactive_slider($id){
        DB::table('tbl_sliders')
                ->where('id',$id)
                ->update(['publication_status'=>1]);
            return redirect()->back();
    }
    public function active_brand($brend_id){
        DB::table('tbl_brand')
               ->where('brand_id',$brend_id)
               ->update(['publication_status'=>0]);
            return redirect()->back();
    }

    public function unactive_brand($brend_id){
        DB::table('tbl_brand')
               ->where('brand_id',$brend_id)
               ->update(['publication_status'=>1]);
            return redirect()->back();
    }

    public function active_staff($id){
        DB::table('tbl_staffs')
               ->where('id',$id)
               ->update(['publication_status'=>0]);
            return redirect()->back();
    }

    public function unactive_staff($id){
        DB::table('tbl_staffs')
               ->where('id',$id)
               ->update(['publication_status'=>1]);
            return redirect()->back();
    }
}
