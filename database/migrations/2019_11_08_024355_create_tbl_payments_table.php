<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_payments', function (Blueprint $table) {
            $table->increments('payment_id');
            $table->string('transaction_id');
            $table->string('payment_method')->nullable();
            $table->string('payment_type')->nullable();
            $table->string('card_number')->nullable();
            $table->string('currency')->nullable();
            $table->double('amount')->nullable();
            $table->string('payment_status')->nullable();
            $table->string('receipt_email')->nullable();
            $table->string('receipt_url')->nullable();
            $table->string('postal_code')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_payments');
    }
}
