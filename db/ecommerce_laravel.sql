-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 18, 2020 at 08:45 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ecommerce_laravel`
--

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2019_09_22_045928_create_tbl_admin_table', 1),
(2, '2019_09_24_082314_create_tbl_category_table', 1),
(3, '2019_10_04_011111_create_tbl_sub_category_table', 1),
(4, '2019_10_13_111625_create_tbl_products_table', 1),
(5, '2019_10_18_134547_create_tbl_sliders_table', 1),
(6, '2019_10_30_130019_create_tbl_customers_table', 1),
(7, '2019_10_31_002453_create_tbl_shippings_table', 1),
(8, '2019_10_31_195610_create_tbl_blog_table', 1),
(9, '2019_11_06_194435_create_tbl_staffs_table', 1),
(10, '2019_11_08_021033_create_tbl_orders_table', 1),
(11, '2019_11_08_021200_create_tbl_order_details_table', 1),
(12, '2019_11_08_024355_create_tbl_payments_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin`
--

CREATE TABLE `tbl_admin` (
  `admin_id` int(10) UNSIGNED NOT NULL,
  `admin_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `admin_email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `admin_password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `admin_phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_admin`
--

INSERT INTO `tbl_admin` (`admin_id`, `admin_name`, `admin_email`, `admin_password`, `admin_phone`, `created_at`, `updated_at`) VALUES
(1, 'Shamim Hassan', 'shamim009@gmail.com', 'shamim009', '01749386163', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_blog`
--

CREATE TABLE `tbl_blog` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `time` datetime NOT NULL,
  `short_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `long_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `publication_status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_brand`
--

CREATE TABLE `tbl_brand` (
  `brand_id` int(10) NOT NULL,
  `brand_name` varchar(191) NOT NULL,
  `brand_description` text NOT NULL,
  `publication_status` int(10) NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_brand`
--

INSERT INTO `tbl_brand` (`brand_id`, `brand_name`, `brand_description`, `publication_status`, `created_at`, `updated_at`) VALUES
(1, 'Samsung', 'SAmsung Mobile', 1, '2020-01-28 14:41:12', '2020-01-28 14:41:12'),
(2, 'Dell', 'Dell', 1, '2020-01-28 15:52:59', '2020-01-28 15:52:59'),
(3, 'Sony', 'Sony', 1, '2020-01-28 16:01:37', '2020-01-28 16:01:37'),
(4, 'Null', 'This Null Brand contains products without Brand', 0, '2020-02-04 06:47:23', '2020-02-04 06:47:23');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_category`
--

CREATE TABLE `tbl_category` (
  `category_id` int(10) UNSIGNED NOT NULL,
  `category_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `publication_status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_category`
--

INSERT INTO `tbl_category` (`category_id`, `category_name`, `category_description`, `publication_status`, `created_at`, `updated_at`) VALUES
(1, 'Electronics', '<p>This is Electronics Category.</p>', 1, NULL, NULL),
(2, 'Automobile', '<p>This is automobile category.</p>', 1, NULL, NULL),
(3, 'Cloths', '<p>This is cloths category.</p>', 0, NULL, NULL),
(4, 'Fashion', '<p>Fashion category.</p>', 1, NULL, NULL),
(5, 'Accessories', '<p>Accessories</p>', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_customers`
--

CREATE TABLE `tbl_customers` (
  `customer_id` int(10) UNSIGNED NOT NULL,
  `customer_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `customer_email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_code` int(10) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_customers`
--

INSERT INTO `tbl_customers` (`customer_id`, `customer_name`, `customer_email`, `password`, `mobile_number`, `address`, `city`, `post_code`, `created_at`, `updated_at`) VALUES
(1, 'Amit Sarker', 'amit009@gmail.com', '2a3097ffe4dc00fcebecdc6ba280a8c9', '01749386163', 'House-12 Road-22 Sector-10 Uttara Dhaka', 'Dhaka', 1203, NULL, NULL),
(2, 'Shamim Hassan', 'towhidshamim009@gmail.com', 'e04c0f117c27e5da529017ec6a60be44', '01749386163', 'House -12 Road -13 Sector -10 Uttara Dhaka', 'Dhaka', 1230, NULL, NULL),
(3, 'Towhedul Islam', 'towhid009@gmail.com', '71ecb20a6b12d9dd0c39daeedeb9cee7', '01723608574', 'Rajendrapur Gazipur', 'Gazipur', 1723, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_orders`
--

CREATE TABLE `tbl_orders` (
  `order_id` int(10) UNSIGNED NOT NULL,
  `customer_id` int(11) NOT NULL,
  `payment_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `total_amount` double NOT NULL,
  `order_date` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_orders`
--

INSERT INTO `tbl_orders` (`order_id`, `customer_id`, `payment_id`, `quantity`, `total_amount`, `order_date`, `status`, `created_at`, `updated_at`) VALUES
(4, 2, 10, 4, 55492.5, '18/03/2020 || 10:29:10 PM', 0, NULL, NULL),
(5, 2, 11, 5, 57435, '18/03/2020 || 11:42:15 PM', 0, NULL, NULL),
(6, 2, 12, 8, 59839.5, '19/03/2020 || 12:43:26 AM', 2, NULL, NULL),
(7, 3, 13, 3, 3958.5, '19/03/2020 || 01:05:11 AM', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_order_details`
--

CREATE TABLE `tbl_order_details` (
  `order_details_id` int(10) UNSIGNED NOT NULL,
  `order_id` int(11) NOT NULL,
  `customer_id` int(10) NOT NULL,
  `shipping_id` int(10) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` double NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_order_details`
--

INSERT INTO `tbl_order_details` (`order_details_id`, `order_id`, `customer_id`, `shipping_id`, `product_id`, `product_name`, `product_image`, `quantity`, `price`, `created_at`, `updated_at`) VALUES
(1, 4, 2, 7, 1, 'Samsung Galaxy M10', 'image/download (5).jpg5e390e55afc22.jpg', 3, 17000, NULL, NULL),
(2, 4, 2, 7, 4, 'Ladies Hand Bag23-1', 'image/download.jpg5e3916bec1556.jpg', 1, 1850, NULL, NULL),
(3, 5, 2, 8, 1, 'Samsung Galaxy M10', 'image/download (5).jpg5e390e55afc22.jpg', 3, 17000, NULL, NULL),
(4, 5, 2, 8, 4, 'Ladies Hand Bag23-1', 'image/download.jpg5e3916bec1556.jpg', 2, 1850, NULL, NULL),
(5, 6, 2, 9, 1, 'Samsung Galaxy M10', 'image/download (5).jpg5e390e55afc22.jpg', 3, 17000, NULL, NULL),
(6, 6, 2, 9, 4, 'Ladies Hand Bag23-1', 'image/download.jpg5e3916bec1556.jpg', 2, 1850, NULL, NULL),
(7, 6, 2, 9, 5, 'Grafea Backpack-203', 'image/images (5).jpg5e3935d520370.jpg', 1, 1390, NULL, NULL),
(8, 6, 2, 9, 7, 'Golf Polo T-Shart', 'image/ad.jpg5e39cb17a02bf.jpg', 2, 450, NULL, NULL),
(9, 7, 3, 11, 3, 'TISSOT Man Watch', 'image/images (1).jpg5e3912a8b0a24.jpg', 1, 990, NULL, NULL),
(10, 7, 3, 11, 5, 'Grafea Backpack-203', 'image/images (5).jpg5e3935d520370.jpg', 2, 1390, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_payments`
--

CREATE TABLE `tbl_payments` (
  `payment_id` int(10) UNSIGNED NOT NULL,
  `transaction_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payment_method` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `card_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `currency` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `payment_status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `receipt_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `receipt_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `postal_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_payments`
--

INSERT INTO `tbl_payments` (`payment_id`, `transaction_id`, `payment_method`, `payment_type`, `card_number`, `currency`, `amount`, `payment_status`, `receipt_email`, `receipt_url`, `postal_code`, `created_at`, `updated_at`) VALUES
(10, 'ch_1GO4lUD8G9nAfnOuO3RE0JfL', 'card_1GO4lTD8G9nAfnOu2hMAei9g', 'VISA', '4242', 'USD', 5549250, 'succeeded', NULL, 'https://pay.stripe.com/receipts/acct_1FbYB9D8G9nAfnOu/ch_1GO4lUD8G9nAfnOuO3RE0JfL/rcpt_Gvwel9dk6HGXglxJ1P1KOniP6xuMOYk', '12345', NULL, NULL),
(11, 'ch_1GO5uDD8G9nAfnOuXUwUbnY9', 'card_1GO5uCD8G9nAfnOu7WjHY6H6', 'VISA', '4242', 'USD', 5743500, 'succeeded', NULL, 'https://pay.stripe.com/receipts/acct_1FbYB9D8G9nAfnOu/ch_1GO5uDD8G9nAfnOuXUwUbnY9/rcpt_Gvxp1ZWDPJtfW2ouk92FvbxLF4Ru9iv', '12345', NULL, NULL),
(12, 'ch_1GO6rRD8G9nAfnOuKAwNMJJ1', 'card_1GO6rQD8G9nAfnOutpsYZNxK', 'VISA', '4242', 'USD', 5983950, 'succeeded', NULL, 'https://pay.stripe.com/receipts/acct_1FbYB9D8G9nAfnOu/ch_1GO6rRD8G9nAfnOuKAwNMJJ1/rcpt_GvypM4VckZdDrHZqGUtQrCGeiTwqimH', '12345', NULL, NULL),
(13, 'ch_1GO7CUD8G9nAfnOu0QSmRvoz', 'card_1GO7CTD8G9nAfnOuzn5h65AX', 'VISA', '4242', 'USD', 395850, 'succeeded', NULL, 'https://pay.stripe.com/receipts/acct_1FbYB9D8G9nAfnOu/ch_1GO7CUD8G9nAfnOu0QSmRvoz/rcpt_GvzAnM66yxox31PuOKnImCaw5U02f4I', '12345', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_products`
--

CREATE TABLE `tbl_products` (
  `product_id` int(10) UNSIGNED NOT NULL,
  `product_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` int(11) NOT NULL,
  `sub_category_id` int(11) NOT NULL,
  `brand_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_short_description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_long_description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_price` double(8,2) NOT NULL,
  `product_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_size` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_color` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_quantity` int(11) NOT NULL,
  `publication_status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_products`
--

INSERT INTO `tbl_products` (`product_id`, `product_name`, `category_id`, `sub_category_id`, `brand_name`, `product_short_description`, `product_long_description`, `product_price`, `product_image`, `product_size`, `product_color`, `product_quantity`, `publication_status`, `created_at`, `updated_at`) VALUES
(1, 'Samsung Galaxy M10', 1, 1, 'Samsung', 'The Samsung Galaxy M10 mobile exhibits a 6.22\" (15.8 cm) display having a screen resolution of 720 x 1520 pixels. Its multi-touch capacitive TFT display is bezel-less with waterdrop notch.', 'Galaxy M10 comes with 3400 mAh typical battery. It has 2 GB RAM, 1.6 GHz octa-core CPU and Mali-T830 MP1 GPU. It is powered by a Exynos 7904 Octa (14 nm) chipset. The phone comes with 16 GB internal storage and dedicated MicroSD slot. There is no fingerprint sensor in this gadget.', 17000.00, 'image/download (5).jpg5e390e55afc22.jpg', '6.22\"(15.8cm)', 'Black,Blue', 2, 1, NULL, NULL),
(2, 'Dell Inspiron 15 3583', 1, 3, 'Dell', 'Inspiron 15 3583 Series Laptop. ... Featuring Intel® processors and an array of ports. ... 8th Generation Intel® Core™ i7-8565U Processor (8MB Cache, up to 4.6 GHz)', 'Dell Premium Inspiron 15 3583 15.6 Inch FHD Laptop (Intel Core i7-8565U up to 4.6 GHz, 8GB RAM, 256GB SSD, Intel UHD Graphics 620, WiFi, Bluetooth, HDMI, Windows 10 Home)   \r\n\r\n               	\r\n New Inspiron 15 3583 Laptop Be the first to review this product. 3583581TB2PS. Regular Price:INR 48200 Out of stock. 8th Generation Intel® Core™ i5-8265U Processor (6MB Cache, up to 3.9 GHz) Microsoft Office Home and Student 2019.', 34000.00, 'image/download (3).jpg5e391021de76f.jpg', '15.6 inch', 'Black, Cyan', 2, 1, NULL, NULL),
(3, 'TISSOT Man Watch', 4, 4, 'Null', 'Shop our blowout sale on Tissot watches. 30 Day Money Back Guarantee. 100% Genuine & Authentic With Original Packaging. Join Our 1 Million+ Satisfied Customers. Trusted Online Since 1997.', 'Tissot SA (French pronunciation: ​[ti\'so]) is a Swiss luxury watchmaker. The company was founded in Le Locle, Switzerland by Charles-Félicien (Madhav) Tissot and his son, Charles-Émile Tissot, in 1853. ... Tissot is not associated with another Swiss watchmaking firm, Mathey-Tissot.', 990.00, 'image/images (1).jpg5e3912a8b0a24.jpg', 'Free, Any Size', 'White, Black, Blue', 2, 1, NULL, NULL),
(4, 'Ladies Hand Bag23-1', 4, 4, 'Null', 'Weight	0.5 kg\r\nDimensions	30 × 20 × 10 cm\r\nItem Type	\r\nHandbags\r\n\r\nShape	\r\nShell\r\n\r\nMain Material	\r\nPU\r\n\r\nHandbags Type	\r\nTotes\r\n\r\nTypes of bags	\r\nHandbags & Crossbody bags\r\n\r\nLining Material	\r\nPolyester\r\n\r\nNumber of Handles/Straps	\r\nTwo\r\n\r\nDecoration	\r\nButton\r\n\r\nGender	\r\nWomen\r\n\r\nPattern Type	\r\nSolid\r\n\r\nClosure Type	\r\nHasp\r\n\r\nHardness	\r\nSoft', 'Interior	\r\nCell Phone Pocket,Interior Zipper Pocket\r\n\r\nExterior	\r\nSilt Pocket\r\n\r\nModel Number	\r\nMS60\r\n\r\nOccasion	\r\nBusiness\r\n\r\nBrand Name	\r\nETALOO\r\n\r\nStyle	\r\nWomen\'s Messenger Bag\r\n\r\nFabric texture	\r\nPU\r\n\r\nMaterial texture	\r\nPolyester\r\n\r\nBags shape	\r\nSquare style\r\n\r\nLuggage trend models	\r\nSmall square bag\r\n\r\npopular elements	\r\nEmbossed\r\n\r\npattern	\r\nSolid color\r\n\r\nProcessing methods	\r\nEmbossed\r\n\r\nAsk carry parts	\r\nSoft handle\r\n\r\nOuter bag type	\r\nInside the patch pocket', 1850.00, 'image/download.jpg5e3916bec1556.jpg', '30 × 20 × 10 cm', 'Black, Red, White', 2, 1, NULL, NULL),
(5, 'Grafea Backpack-203', 4, 4, 'Null', 'Weight	0.5 kg\r\nDimensions	30 × 20 × 10 cm\r\nItem Type	\r\nHandbags\r\n\r\nShape	\r\nShell\r\n\r\nMain Material	\r\nPU\r\n\r\nHandbags Type	\r\nTotes\r\n\r\nTypes of bags	\r\nHandbags & Crossbody bags\r\n\r\nLining Material	\r\nPolyester\r\n\r\nNumber of Handles/Straps	\r\nTwo\r\n\r\nDecoration	\r\nButton\r\n\r\nGender	\r\nWomen\r\n\r\nPattern Type	\r\nSolid\r\n\r\nClosure Type	\r\nHasp\r\n\r\nHardness	\r\nSoft', 'Interior	\r\nCell Phone Pocket,Interior Zipper Pocket\r\n\r\nExterior	\r\nSilt Pocket\r\n\r\nModel Number	\r\nMS60\r\n\r\nOccasion	\r\nBusiness\r\n\r\nBrand Name	\r\nETALOO\r\n\r\nStyle	\r\nWomen\'s Messenger Bag\r\n\r\nFabric texture	\r\nPU\r\n\r\nMaterial texture	\r\nPolyester\r\n\r\nBags shape	\r\nSquare style\r\n\r\nLuggage trend models	\r\nSmall square bag\r\n\r\npopular elements	\r\nEmbossed\r\n\r\npattern	\r\nSolid color\r\n\r\nProcessing methods	\r\nEmbossed\r\n\r\nAsk carry parts	\r\nSoft handle\r\n\r\nOuter bag type	\r\nInside the patch pocket', 1390.00, 'image/images (5).jpg5e3935d520370.jpg', '30 × 20 × 10 cm', 'Black, Red, White', 2, 1, NULL, NULL),
(6, 'Bajaj Pulsar NS160', 2, 2, 'Null', 'Engine\r\nEngine CC160.3 cc\r\nNo Of Cylinder1\r\nMax Power15.2 bhp @ 8500 rpm\r\nMax Torque14.6 Nm @ 6500 rpm\r\nValves Per Cylinder2\r\nFuel DeliveryCarbuerator\r\nCooling SystemOil Cooled\r\nStarting MechanismKick / Self Start', 'Dimension and Weight\r\n\r\nKerb Weight142 kg\r\nLength2012 mm\r\nWidth804 mm\r\nHeight1060 mm\r\nWheelbase1363 mm\r\nGround Clearance176 mm\r\nSeat Height805 mm\r\nChassis and Suspension\r\nChassis TypePerimeter Frame\r\nFront Suspension130mm fork travel, Telescopic\r\nRear Suspension120mm wheel travel, Mono suspension with nitrox', 230000.00, 'image/download (11).jpg5e393acd19900.jpg', 'Normal', 'Red, Black, Blue', 2, 1, NULL, NULL),
(7, 'Golf Polo T-Shart', 3, 4, 'Null', 'This short sleeve polo shirt is made of 100% cotton; perfect for comfort, toughness and durability.', 'This short sleeve polo shirt is made of 100% cotton; perfect for comfort, toughness and durability.', 450.00, 'image/ad.jpg5e39cb17a02bf.jpg', 'S,M,L,XL', 'Red,Black,Blue,Green', 10, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_shippings`
--

CREATE TABLE `tbl_shippings` (
  `shipping_id` int(10) UNSIGNED NOT NULL,
  `shipping_email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `shipping_first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `shipping_last_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `shipping_address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `shipping_mobile_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `shipping_city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_shippings`
--

INSERT INTO `tbl_shippings` (`shipping_id`, `shipping_email`, `shipping_first_name`, `shipping_last_name`, `shipping_address`, `shipping_mobile_number`, `shipping_city`, `created_at`, `updated_at`) VALUES
(7, 'shamim009@gmail.com', 'Shamim', 'Hassan', 'House 12 Road -13 Baya Rajshahi', '01862827975', 'Rajshahi', NULL, NULL),
(8, 'amit009@gmail.com', 'Amit', 'Sarker', 'Muktagasha Mymenshing', '01862827975', 'Mymenshing', NULL, NULL),
(9, 'towhedul009@gmail.com', 'Ashik', 'Hassan', 'House 12 Road -13 Joldhaka Nilfamari', '01862827975', 'Nilfamari', NULL, NULL),
(10, 'amdadul009@gmail', 'Amdadul', 'Islam', 'House 12 Road -13 Sheepbari Gazipur', '01862827975', 'Gazipur', NULL, NULL),
(11, 'amdadul009@gmail', 'Amdadul', 'Islam', 'House 12 Road -13 Sheepbari Gazipur', '01862827975', 'Gazipur', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_sliders`
--

CREATE TABLE `tbl_sliders` (
  `id` int(10) UNSIGNED NOT NULL,
  `image_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_subtitle` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slider_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `publication_status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_sliders`
--

INSERT INTO `tbl_sliders` (`id`, `image_title`, `image_subtitle`, `slider_image`, `publication_status`, `created_at`, `updated_at`) VALUES
(1, 'Get Product Without Shipping Cost', 'Choose your product and ordered it and Get without shipping cos.', 'girl1.jpg-5dabd4d237579.jpg-5e304be57ed81.jpg', 1, NULL, NULL),
(2, 'Choose 100% real product', 'We have lots of product that are absolutely real product.', 'girl2.jpg-5dc33ef21c20e.jpg-5e304deddad25.jpg', 1, NULL, NULL),
(3, 'Choose Your Best one', 'Search here and collect your best product.', 'girl3.png-5dabd9188dab2.png-5e304fb0eff87.png', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_staffs`
--

CREATE TABLE `tbl_staffs` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `area` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `publication_status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_staffs`
--

INSERT INTO `tbl_staffs` (`id`, `name`, `email`, `address`, `area`, `mobile_number`, `image`, `publication_status`, `created_at`, `updated_at`) VALUES
(1, 'Samnta', 'san94@gmail.com', 'Dhanmondi, Dhaka', 'Dhanmondi, Dhaka', '01732036907', 'iframe2.png-5dc352caab56f.png-5e305c9213620.png', 1, NULL, NULL),
(2, 'Samnta', 'san94@gmail.com', 'Dhanmondi, Dhaka', 'Dhanmondi, Dhaka', '01732036907', 'gallery3.jpg-5dc336c425f36.jpg-5e305d52142bb.jpg', 1, NULL, NULL),
(3, 'Samnta', 'san94@gmail.com', 'Dhanmondi, Dhaka', 'Dhanmondi, Dhaka', '01732036907', 'iframe3.png-5dc3521489ca5.png-5e305debd4c8e.png', 0, NULL, NULL),
(4, 'Samnta', 'san94@gmail.com', 'Dhanmondi, Dhaka', 'Dhanmondi, Dhaka', '01732036907', 'iframe4.png-5e307ae4af0a4.png', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_sub_category`
--

CREATE TABLE `tbl_sub_category` (
  `sub_category_id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `sub_category_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sub_category_description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `publication_status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_sub_category`
--

INSERT INTO `tbl_sub_category` (`sub_category_id`, `category_id`, `sub_category_name`, `sub_category_description`, `publication_status`, `created_at`, `updated_at`) VALUES
(1, 1, 'Mobile', 'This is Mobile subcategory under Electronics.', 1, NULL, NULL),
(2, 2, 'Motorcycle', 'This is Motorcycle under Automobile category.', 1, NULL, NULL),
(3, 1, 'Computer', 'This is Computer subcategory under Electronics.', 1, NULL, NULL),
(4, 4, 'Watch', 'Subcategory Watch.', 1, NULL, NULL),
(5, 5, 'Computer Accessories', 'Computer Accessories', 0, NULL, NULL),
(6, 5, 'Mobile Accessories', 'Mobile Accessories', 0, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `tbl_blog`
--
ALTER TABLE `tbl_blog`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_brand`
--
ALTER TABLE `tbl_brand`
  ADD PRIMARY KEY (`brand_id`);

--
-- Indexes for table `tbl_category`
--
ALTER TABLE `tbl_category`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `tbl_customers`
--
ALTER TABLE `tbl_customers`
  ADD PRIMARY KEY (`customer_id`);

--
-- Indexes for table `tbl_orders`
--
ALTER TABLE `tbl_orders`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `tbl_order_details`
--
ALTER TABLE `tbl_order_details`
  ADD PRIMARY KEY (`order_details_id`);

--
-- Indexes for table `tbl_payments`
--
ALTER TABLE `tbl_payments`
  ADD PRIMARY KEY (`payment_id`);

--
-- Indexes for table `tbl_products`
--
ALTER TABLE `tbl_products`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `tbl_shippings`
--
ALTER TABLE `tbl_shippings`
  ADD PRIMARY KEY (`shipping_id`);

--
-- Indexes for table `tbl_sliders`
--
ALTER TABLE `tbl_sliders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_staffs`
--
ALTER TABLE `tbl_staffs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_sub_category`
--
ALTER TABLE `tbl_sub_category`
  ADD PRIMARY KEY (`sub_category_id`),
  ADD KEY `tbl_sub_category_category_id_foreign` (`category_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  MODIFY `admin_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_blog`
--
ALTER TABLE `tbl_blog`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_brand`
--
ALTER TABLE `tbl_brand`
  MODIFY `brand_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_category`
--
ALTER TABLE `tbl_category`
  MODIFY `category_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_customers`
--
ALTER TABLE `tbl_customers`
  MODIFY `customer_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_orders`
--
ALTER TABLE `tbl_orders`
  MODIFY `order_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tbl_order_details`
--
ALTER TABLE `tbl_order_details`
  MODIFY `order_details_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `tbl_payments`
--
ALTER TABLE `tbl_payments`
  MODIFY `payment_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `tbl_products`
--
ALTER TABLE `tbl_products`
  MODIFY `product_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tbl_shippings`
--
ALTER TABLE `tbl_shippings`
  MODIFY `shipping_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `tbl_sliders`
--
ALTER TABLE `tbl_sliders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_staffs`
--
ALTER TABLE `tbl_staffs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_sub_category`
--
ALTER TABLE `tbl_sub_category`
  MODIFY `sub_category_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl_sub_category`
--
ALTER TABLE `tbl_sub_category`
  ADD CONSTRAINT `tbl_sub_category_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `tbl_category` (`category_id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
