@extends('layout')

@section('title','Card')

@section('content')

<div class="col-sm-9">
<section id="cart_items">
		
			<div class="breadcrumbs">
				<ol class="breadcrumb">
				  <li><a href="#">Home</a></li>
				  <li class="active">Shopping Cart</li>
				</ol>
			</div>			

                <?php 
                     $empty = Cart::isEmpty();
                 ?>
                @if($empty)
                    <div class="content">
                    	<h2 style="color:#eb7d34;font-style: italic;font-weight: bold;">Nothing Added Yeat...</h2>
						<img src="{{asset('frontend/images/404/empty.svg')}}" alt="" width="100%" height="400px">
					</div>
                    
               		           
                @else
                <div class="table-responsive cart_info">
				<table class="table table-condensed">

                <?php 
                      $cartCollection = Cart::getContent();

                     /*echo "<pre>";
                         print_r($cartCollection);
                      echo"</pre>";
                      exit();  */

                   $condition = new \Darryldecode\Cart\CartCondition(array(
			            'name' => 'VAT 5%',
			            'type' => 'tax',
			            'target' => 'total', // this condition will be applied to cart's subtotal when getSubTotal() is called.
			            'value' => '5%',
			            'attributes' => array( // attributes field is optional
			                'description' => 'Value added tax',
			                'more_data' => 'more data here'
			            )
			        ));
                   
                Cart::condition($condition);
                $cartConditions = Cart::getConditions();

                foreach($cartConditions as $condition)
				{
				    $condition->getTarget(); // the target of which the condition was applied
				    $condition->getName(); // the name of the condition
				    $condition->getType(); // the type
				    $condition->getValue(); // the value of the condition
				    $condition->getOrder(); // the order of the condition
				    $condition->getAttributes(); // the attributes of the condition, returns an empty [] if no attributes added
				}

				$subTotal = Cart::getSubTotal();
				$condition = Cart::getCondition('VAT 5%');
				$conditionCalculatedValue = $condition->getCalculatedValue($subTotal);

                                     
                ?>
            
					<thead>
						<tr class="cart_menu">
							<td class="image">Image</td>
							<td class="description">Name</td>
							<td class="price">Price</td>
							<td class="quantity">Quantity</td>
							<td class="total">Total</td>
							<td>Action</td>
						</tr>
					</thead>
					<tbody>

				@foreach($cartCollection as $card)		
						<tr>
							<td class="cart_product">
								<a href=""><img src="{{URL::to($card->attributes->image)}}" alt="" width="80px" height="80px";></a>
							</td>
							<td class="cart_description">
								<h4><a href="">{{$card->name}}</a></h4>
								
							</td>
							<td class="cart_price">
								<p>{{$card->price}}</p>
							</td>
							<td class="cart_quantity">
								<div class="cart_quantity_button">
									<a class="cart_quantity_up" href="{{URL::to('/increment_quantity/'.$card->id)}}"> + </a>
                            
									<input class="cart_quantity_input" type="text" name="quantity" value="{{$card->quantity}}" autocomplete="off" size="2">

									<a class="cart_quantity_down" href="{{URL::to('/decrement_quantity/'.$card->id)}}"> - </a>
								</div>
							</td>
							<td class="cart_total">
								<p class="cart_total_price">{{ $card->price * $card->quantity}}</p>

								
							</td>
							<td class="cart_delete">
								<a class="cart_quantity_delete" href="{{URL::to('/delete_to_cart/'.$card->id)}}"><i class="fa fa-times"></i></a>
							</td>
						</tr>

				@endforeach		
						
					</tbody>
				</table>
			</div>
		
	</section> <!--/#cart_items-->

	<section id="do_action">
		
			<div class="heading">
				<h3>What would you like to do next?</h3>
				<p>Choose if you have a discount code or reward points you want to use or would like to estimate your delivery cost.</p>
			</div>
			<div class="row">

				<div class="col-sm-8">
					<div class="total_area">
						<ul>
						
							<li>Cart Sub Total <span>{{Cart::getSubTotal()}}</span></li>
							<li>Tax ({{$condition->getValue()}}) <span>{{$conditionCalculatedValue}}</span></li>
							<li>Shipping Cost <span>Free</span></li>
							<li>Total <span>{{Cart::getTotal()}}</span></li>

						</ul>
							<a class="btn btn-default update" href="{{url('/')}}">Continue Shopping</a>

                        <?php 

                           $customer_id=Session::get('customer_id');
                           /*echo $customer_id;*/
                        ?>

                        @if(!empty($customer_id))
                            <a class="btn btn-default check_out" href="{{URL::to('/checkout')}}">Check Out</a>
                        @else

							<a class="btn btn-default check_out" href="{{URL::to('/login_check')}}">Check Out</a>
						@endif
					</div>
				</div>
			</div>
		
	</section><!--/#do_action-->
                

                @endif
                
</div>
	


@endsection


