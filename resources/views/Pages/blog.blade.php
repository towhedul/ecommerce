@extends('layout')
@section('title','Blog')


@section('content')

<div class="col-sm-9">
					<div class="blog-post-area">
						<h2 class="title text-center">Latest From our Blog</h2>

                <?php 

                     $blogs=DB::table('tbl_blog')
                           ->where('publication_status',1)
                           ->get();

                ?>

                    @foreach($blogs as $blog)

						<div class="single-blog-post">
							<h3>{{$blog->title}}</h3>
							<div class="post-meta">
								<ul>
									<!-- <li><i class="fa fa-user"></i> Mac Doe</li> -->
									<li><i class="fa fa-clock-o"></i> {{date("g:i A", strtotime($blog->time))}}</li>
									<li><i class="fa fa-calendar"></i> {{date("M d, Y", strtotime($blog->date))}}</li>
								</ul>
								<span>
										<i class="fa fa-star"></i>
										<i class="fa fa-star"></i>
										<i class="fa fa-star"></i>
										<i class="fa fa-star"></i>
										<i class="fa fa-star-half-o"></i>
								</span>
							</div>
							<a href="">
								<img src="{{asset('upload/blog/'.$blog->blog_image)}}" alt="" style="height:350px; width:100%">
							</a>
							<p>{{$blog->short_description}}</p>
							<a  class="btn btn-primary" href="{{URL::to('blog_details/'.$blog->id)}}">Read More</a>
						</div>
                    @endforeach


						
						<div class="pagination-area">
							<ul class="pagination">
								<li><a href="" class="active">1</a></li>
								<li><a href="">2</a></li>
								<li><a href="">3</a></li>
								<li><a href=""><i class="fa fa-angle-double-right"></i></a></li>
							</ul>
						</div>
					</div>
				</div>

@endsection