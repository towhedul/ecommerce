@extends('layout')

@section('title','Checkout')

@section('content')
<div class="col-sm-9">
<section id="cart_items">
		<div class="container">
			
			<div class="register-req col-sm-8">
				<p>Please fillup the below information where you want to deliver your product.</p>
			</div><!--/register-req-->

			<div class="shopper-informations">
				<div class="row">
					
					<div class="col-sm-10 clearfix">
						<div class="bill-to">
							<p>Shipping Details</p>
							<div class="form-one">
								<form action="{{url('/shipping_details')}}" method="POST">
								  @csrf	
									<input type="text" name="shipping_email" 
									placeholder="Email *">			
									<input type="text" name="shipping_first_name" placeholder="First Name *">
								    <input type="text" name="shipping_last_name" placeholder="Last Name *">
									<input type="text" name="shipping_address" placeholder="Address *">
									<input type="text" name="shipping_mobile_number" placeholder="Mobile Number">
									<input type="text" name="shipping_city" 
									placeholder="City *">
                                    <input type="submit" class="btn btn-default" name="submit" value="Done">
									
								</form>
								
							</div>
							
						</div>
					</div>
										
				</div>
			</div>
			<div class="review-payment">
				<h2>Review & Payment</h2>
			</div>

		</div>
	</section> <!--/#cart_items-->
</div>

@endsection