@extends('layout')

@section('title','Home | E-Shopper')

@section('content')

<div class="col-sm-9 padding-right">
                    <div class="features_items"><!--features_items-->
                        <h2 class="title text-center">Features Items</h2>

            
        @foreach($all_published_products as $key=>$product)



                        <div class="col-sm-4">
                            <div class="product-image-wrapper">
                                <div class="single-products">
                                        <div class="productinfo text-center">
                                            <img src="{{URL::to($product->product_image)}}"alt=""  style="width:160px;height:180px;"/>
                                            <h2>&#2547; {{$product->product_price}}</h2>
                                            <p>{{$product->product_name}}</p>
                                            <a href="{{URL::to('product_details/'.$product->product_id)}}" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                                        </div>
                                        
                                </div>
                                <div class="choose">
                                    <ul class="nav nav-pills nav-justified">
                                        <li><a href="#"><i class="fa fa-plus-square"></i>Add to wishlist</a></li>
                                        <li><a href="{{URL::to('product_details/'.$product->product_id)}}"><i class="fa fa-plus-square"></i>View Details</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        
                        
            @endforeach

                        
                    </div><!--features_items-->
                    
                    <div class="category-tab"><!--category-tab-->
                        <div class="col-sm-12">
                            <ul class="nav nav-tabs">

            <?php 
                 
                  $all_category=DB::table('tbl_category')
                   ->where('publication_status',1)
                   ->get();


            ?>
           

                    @foreach($all_category as $key=>$category)

                                <li class=""><a href="#{{$category->category_name}}" data-toggle="tab">{{$category->category_name}}</a></li>

                    @endforeach            
                            </ul>
                        </div>
                        <div class="tab-content">

                    @foreach($all_category as $key=>$category)
                        <div class="tab-pane fade active in" id="{{$category->category_name}}" >

                        @foreach($all_published_products as $key=>$product)  
                           @if($category->category_name == $product->category_name)
                            <div class="col-sm-4">
                                <div class="product-image-wrapper">
                                    <div class="single-products">
                                        <div class="productinfo text-center">
                                            <img src="{{URL::to($product->product_image)}}"alt=""  style="width:160px;height:180px;"/>
                                            <h2>&#2547; {{$product->product_price}}</h2>
                                            <div class="productinfo">
                                             <p>{{$product->product_name}}</p>   
                                            </div>
                                            
                                            <a href="{{URL::to('product_details/'.$product->product_id)}}" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                            @else
                            @endif
                        @endforeach
                                                            
                        </div>

                    @endforeach


                        </div>
                    </div><!--/category-tab-->
                    
                    @include('Pages.partial.recomended')<!--/recommended_items-->
                    
                </div>

@endsection