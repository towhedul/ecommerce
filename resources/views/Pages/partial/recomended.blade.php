<div class="recommended_items"><!--recommended_items-->
    <h2 class="title text-center">recommended items</h2>    

     <?php 

          $products = DB::table('tbl_products')                      
                      ->where('publication_status',1)
                      ->get()
                      ->random(3);
                    
      ?>

   
    <div id="recommended-item-carousel" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
            <div class="item active"> 
            @foreach($products as $key=>$product) 
                <div class="col-sm-4">
                    <div class="product-image-wrapper">
                        <div class="single-products">
                            <div class="productinfo text-center">
                                <img src="{{URL::to($product->product_image)}}"alt=""  style="width:130px;height:150px;"/>
                                <h2>&#2547; {{$product->product_price}}</h2>
                                <p>{{$product->product_name}}</p>
                                <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                            </div>
                            
                        </div>
                    </div>
                </div>
                @endforeach                
                
            </div>

            <?php 

              $products_all = DB::table('tbl_products')
                          ->where('publication_status',1)
                          ->get()
                          ->random(3);
                        
              ?>
            <div class="item">  
                @foreach($products_all as $key=>$product)  
                <div class="col-sm-4">
                    <div class="product-image-wrapper">
                        <div class="single-products">
                            <div class="productinfo text-center">
                                <img src="{{URL::to($product->product_image)}}"alt=""  style="width:130px;height:150px;"/>
                                <h2>&#2547; {{$product->product_price}}</h2>
                                <p>{{$product->product_name}}</p>
                                <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                            </div>
                            
                        </div>
                    </div>
                </div>
                @endforeach  
                
            </div>
        </div>
         <a class="left recommended-item-control" href="#recommended-item-carousel" data-slide="prev">
            <i class="fa fa-angle-left"></i>
          </a>
          <a class="right recommended-item-control" href="#recommended-item-carousel" data-slide="next">
            <i class="fa fa-angle-right"></i>
          </a>          
    </div>
</div>