@extends('layout')


@section('content')
<div class="col-sm-9">
<section id="do_action">
		
			<div class="heading">
				<h3>What would you like to do next?</h3>
				<p>Choose if you have a discount code or reward points you want to use or would like to estimate your delivery cost.</p>
			</div>

			<div class="row">

			<!--	
				<div class="col-sm-6">
					<div class="chose_area">
						<ul class="user_option">
							<li>
								<input type="checkbox">
								<label>Use Coupon Code</label>
							</li>
							<li>
								<input type="checkbox">
								<label>Use Gift Voucher</label>
							</li>
							<li>
								<input type="checkbox">
								<label>Estimate Shipping & Taxes</label>
							</li>
						</ul>
						<ul class="user_info">
							<li class="single_field">
								<label>Country:</label>
								<select>
									<option>United States</option>
									<option>Bangladesh</option>
									<option>UK</option>
									<option>India</option>
									<option>Pakistan</option>
									<option>Ucrane</option>
									<option>Canada</option>
									<option>Dubai</option>
								</select>
								
							</li>
							<li class="single_field">
								<label>Region / State:</label>
								<select>
									<option>Select</option>
									<option>Dhaka</option>
									<option>London</option>
									<option>Dillih</option>
									<option>Lahore</option>
									<option>Alaska</option>
									<option>Canada</option>
									<option>Dubai</option>
								</select>
							
							</li>
							<li class="single_field zip-field">
								<label>Zip Code:</label>
								<input type="text">
							</li>
						</ul>
						<a class="btn btn-default update" href="">Get Quotes</a>
						<a class="btn btn-default check_out" href="">Continue</a>
					</div>
				</div>

            -->
            <?php 
                      $cartCollection = Cart::getContent();

                     /*echo "<pre>";
                         print_r($cartCollection);
                      echo"</pre>";
                      exit();  */

                   $condition = new \Darryldecode\Cart\CartCondition(array(
			            'name' => 'VAT 5%',
			            'type' => 'tax',
			            'target' => 'total', // this condition will be applied to cart's subtotal when getSubTotal() is called.
			            'value' => '5%',
			            'attributes' => array( // attributes field is optional
			                'description' => 'Value added tax',
			                'more_data' => 'more data here'
			            )
			        ));
                   
                Cart::condition($condition);
                $cartConditions = Cart::getConditions();

                foreach($cartConditions as $condition)
				{
				    $condition->getTarget(); // the target of which the condition was applied
				    $condition->getName(); // the name of the condition
				    $condition->getType(); // the type
				    $condition->getValue(); // the value of the condition
				    $condition->getOrder(); // the order of the condition
				    $condition->getAttributes(); // the attributes of the condition, returns an empty [] if no attributes added
				}

				$subTotal = Cart::getSubTotal();
				$condition = Cart::getCondition('VAT 5%');
				$conditionCalculatedValue = $condition->getCalculatedValue($subTotal);

                                     
                ?>


				<div class="col-sm-8">
					<div class="total_area">
						<ul>
							<li>Cart Sub Total <span>{{Cart::getSubTotal()}}</span></li>
							<li>Eco Tax <span>{{$conditionCalculatedValue}}</span></li>
							<li>Shipping Cost <span>Free</span></li>
							<li>Total <span>{{Cart::getTotal()}}</span></li>
						</ul>
							
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-8">
					<h3 class="text-center">Selece Your Payment Method</h3>
				</div>
				<div class="col-sm-8">
					<div class="total_area">
						
						<a href="{{url('/payment_by_paypal')}}"><img src="{{asset('image/paypal.png')}}" alt="" style="float: left;
								    width: 30%;
								    height: 100px;
								    border: 3px solid #E0E0E0;
								    margin: 5px;"></a>	
						<a href="{{url('/payment_success')}}"><img src="{{asset('image/strip.png')}}" style="width: 30%;
							    height: 104px;
							    border: 2px solid #E0E0E0;"></a>

						<a href="{{url('/payment_by_authorize')}}"><img src="{{asset('image/anet.png')}}" style="width: 30%;
							    height: 104px;
							    border: 2px solid #E0E0E0;"></a>	    
						
						<a href=""><img src="{{asset('image/ssl.jpg')}}" style="width: 30%;
							    height: 104px;
							    border: 2px solid #E0E0E0;"></a>	
							
					</div>
				</div>
                
			
			</div>	
		
	</section>
</div>
	

@endsection


