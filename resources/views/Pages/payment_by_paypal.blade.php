<?php 
$total=Cart::getTotal();
$name=Session::get('customer_name');
/*echo $total;*/
/*echo $name;*/

 ?>

@extends('layout')
@section('title','Payment By Paypal')

@section('content')

<div class="col-sm-9">
<section id="cart_items">
		

			<div class="table-responsive cart_info">
				<table class="table table-condensed">

                <?php 
                      $cartCollection = Cart::getContent();

                      /*echo "<pre>";
                         print_r($cartCollection);
                      echo"</pre>";
                      exit(); */                
                ?>
            
					<thead>
						<tr class="cart_menu">
							<td class="image">Image</td>
							<td class="description">Name</td>
							<td class="price">Price</td>
							<td class="quantity">Quantity</td>
							<td class="total">Total</td>
						</tr>
					</thead>
					<tbody>

				@foreach($cartCollection as $card)		
						<tr>
							<td class="cart_product">
								<a href=""><img src="{{URL::to($card->attributes->image)}}" alt="" width="60px" height="40px";></a>
							</td>
							<td class="cart_description">
								<h4><a href="">{{$card->name}}</a></h4>
								
							</td>
							<td class="cart_price">
								<p>{{$card->price}}</p>
							</td>
							<td class="cart_quantity">
								<div class="cart_quantity_button">
									
                            
									<input class="cart_quantity_input" type="text" name="quantity" value="{{$card->quantity}}" autocomplete="off" size="2">

									
								</div>
							</td>
							<td class="cart_total">
								<p class="cart_total_price">{{ $card->price * $card->quantity}}</p>

								
							</td>
							
						</tr>

				@endforeach		
						
					</tbody>
				</table>
			</div>
		
	</section>

	<section id="do_action">

			<div class="row">
                 
               <div class="col-sm-5">
               	<h3>Payment Details</h3>
					<div class="">
						
						<form class="" action="{{url('/paypal_payment')}}" method="POST" id="payment-form">
							@csrf
						  <div class="form-group">
						  	<label for="Name">NAME ON CARD</label>
						      
						    <input type="text" class="form-control" id="name"
						     name="name" value="" Placeholder="Enter Name">
						  </div>

						  <div class="form-group">
						  	<label for="Email">Email</label>
						      
						    <input type="email" class="form-control" id="email"
						     name="email" value="" Placeholder="Enter Email">
						  </div> 

						                        
						  <!-- <div class="row">
						  				<div class="col-12">
						  				<div class="left">
						  					<div class="form-group">
						  						<label for="Expiry Date">Expiry Date</label>
						  							<div class="col-3">
						  								<input type="text"  name="" value="">
						  							</div>
						  							<div class="col-3">
						  								<input type="text"  name="" value="">
						  							</div>	
						  						
						  					</div>
						  				</div>
						  				<div class="right">
						  					
						  				</div>
						  				</div>
						  			</div> 
						  			 			 -->			
						    


						    <br>
						    <div class="form-group">
						    	<input type="submit" class="btn btn-success form-control" name="button" value="Paypal Payment">		
						    </div>
						    
						    
						 </form>
						
					</div>
				</div>

				<div class="col-sm-5 pull-right">
					<div class="total_area">
						<ul>
							<li>Cart Sub Total <span>{{Cart::getSubTotal()}}</span></li>
							<li>Eco Tax <span>0.00</span></li>
							<li>Shipping Cost <span>Free</span></li>
							<li>Total <span>{{Cart::getTotal()}}</span></li>
						</ul>
							
					</div>
				</div>
			</div>
		
	</section>
	
</div>

@endsection