<?php 
$total=Cart::getTotal();
$name=Session::get('customer_name');
/*echo $total;*/
/*echo $name;*/

 ?>

 @extends('layout')

 @section('title','Order')

 @push('css')
  <script src="https://js.stripe.com/v3/"></script>

  <style type="text/css" media="screen">
  	/**
 * The CSS shown here will not be introduced in the Quickstart guide, but shows
 * how you can use CSS to style your Element's container.
 */
.StripeElement {
  box-sizing: border-box;

  height: 40px;

  padding: 10px 12px;

  border: 1px solid #CCCCCC; 
  border-radius: 4px;
  background-color: white;

  box-shadow: 0 1px 3px 0 #e6ebf1;
  -webkit-transition: box-shadow 150ms ease;
  transition: box-shadow 150ms ease;
}

.StripeElement--focus {
  box-shadow: 0 1px 3px 0 #66AFE9;
}

.StripeElement--invalid {
  border-color: #fa755a;
}

.StripeElement--webkit-autofill {
  background-color: #fefde5 !important;
}
#card-errors{
	color:#fa755a;
}
  </style>

 @endpush

 @section('content')


 <div class="col-sm-9">
<section id="cart_items">
		

			<div class="table-responsive cart_info">
				<table class="table table-condensed">

                <?php 
                      $cartCollection = Cart::getContent();

                      /*echo "<pre>";
                         print_r($cartCollection);
                      echo"</pre>";
                      exit(); */                
                ?>
            
					<thead>
						<tr class="cart_menu">
							<td class="image">Image</td>
							<td class="description">Name</td>
							<td class="price">Price</td>
							<td class="quantity">Quantity</td>
							<td class="total">Total</td>
						</tr>
					</thead>
					<tbody>

				@foreach($cartCollection as $card)		
						<tr>
							<td class="cart_product">
								<a href=""><img src="{{URL::to($card->attributes->image)}}" alt="" width="60px" height="40px";></a>
							</td>
							<td class="cart_description">
								<h4><a href="">{{$card->name}}</a></h4>
								
							</td>
							<td class="cart_price">
								<p>{{$card->price}}</p>
							</td>
							<td class="cart_quantity">
								<div class="cart_quantity_button">
									
                            
									<input class="cart_quantity_input" type="text" name="quantity" value="{{$card->quantity}}" autocomplete="off" size="2">

									
								</div>
							</td>
							<td class="cart_total">
								<p class="cart_total_price">{{ $card->price * $card->quantity}}</p>

								
							</td>
							
						</tr>

				@endforeach		
						
					</tbody>
				</table>
			</div>
		
	</section> <!--/#cart_items-->

	<section id="do_action">

			<div class="row">
                 
               <div class="col-sm-7">
               	<h3>Payment Details</h3>
					<div class="">
						
						<form class="" action="{{url('/checkout')}}" method="POST" id="payment-form">
							@csrf
						  <div class="form-group">
						  	<label for="Name">Name on Card</label>
						      
						    <input type="text" class="form-control" id="name"
						     name="name" value="">
						  </div>   
						   						
						    <div class="form-group">
						      <label for="card-element">
								      Credit or debit card
								    </label>
								    <div id="card-element">
								      <!-- A Stripe Element will be inserted here. -->
								    </div>

								    <!-- Used to display form errors. -->
								    <div id="card-errors" role="alert"></div>
						      
						    </div>


						    <br>
						    <div class="form-group">
						    	<input type="submit" class="btn btn-info form-control" name="button" value="Complete Order">		
						    </div>
						    
						    
						 </form>
						
					</div>
				</div>

				<?php 
                      $cartCollection = Cart::getContent();

                     /*echo "<pre>";
                         print_r($cartCollection);
                      echo"</pre>";
                      exit();  */

                   $condition = new \Darryldecode\Cart\CartCondition(array(
			            'name' => 'VAT 5%',
			            'type' => 'tax',
			            'target' => 'total', // this condition will be applied to cart's subtotal when getSubTotal() is called.
			            'value' => '5%',
			            'attributes' => array( // attributes field is optional
			                'description' => 'Value added tax',
			                'more_data' => 'more data here'
			            )
			        ));
                   
                Cart::condition($condition);
                $cartConditions = Cart::getConditions();

                foreach($cartConditions as $condition)
				{
				    $condition->getTarget(); // the target of which the condition was applied
				    $condition->getName(); // the name of the condition
				    $condition->getType(); // the type
				    $condition->getValue(); // the value of the condition
				    $condition->getOrder(); // the order of the condition
				    $condition->getAttributes(); // the attributes of the condition, returns an empty [] if no attributes added
				}

				$subTotal = Cart::getSubTotal();
				$condition = Cart::getCondition('VAT 5%');
				$conditionCalculatedValue = $condition->getCalculatedValue($subTotal);

                                     
                ?>

				<div class="col-sm-5">
					<div class="total_area">
						<ul>
							<li>Cart Sub Total <span>{{Cart::getSubTotal()}}</span></li>
							<li>Tax ({{$condition->getValue()}})<span>{{$conditionCalculatedValue}}</span></li>
							<li>Shipping Cost <span>Free</span></li>
							<li>Total <span>{{Cart::getTotal()}}</span></li>
						</ul>
							
					</div>
				</div>
			</div>
		
	</section><!--/#do_action-->
</div>
	
 @endsection



 @push('scripts')
 <script >

(function(){

   // Create a Stripe client.
var stripe = Stripe('pk_test_wBMGLwGmqIhiqH1XKE7cqnFb00dgqPK2CF');

// Create an instance of Elements.
var elements = stripe.elements();

// Custom styling can be passed to options when creating an Element.
// (Note that this demo uses a wider set of styles than the guide below.)
var style = {
  base: {
    color: '#32325d',
    fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
    fontSmoothing: 'antialiased',
    fontSize: '16px',
    '::placeholder': {
      color: '#aab7c4'
    }
  },
  invalid: {
    color: '#fa755a',
    iconColor: '#fa755a'
  }
};

// Create an instance of the card Element.
var card = elements.create('card', {style: style});

// Add an instance of the card Element into the `card-element` <div>.
card.mount('#card-element');

// Handle real-time validation errors from the card Element.
card.addEventListener('change', function(event) {
  var displayError = document.getElementById('card-errors');
  if (event.error) {
    displayError.textContent = event.error.message;
  } else {
    displayError.textContent = '';
  }
});

// Handle form submission.
var form = document.getElementById('payment-form');
form.addEventListener('submit', function(event) {
  event.preventDefault();


  var options = {

     name: document.getElementById('name').value

  }

  stripe.createToken(card,options).then(function(result) {
    if (result.error) {
      // Inform the user if there was an error.
      var errorElement = document.getElementById('card-errors');
      errorElement.textContent = result.error.message;
    } else {
      // Send the token to your server.
      stripeTokenHandler(result.token);
    }
  });
});

// Submit the form with the token ID.
function stripeTokenHandler(token) {
  // Insert the token ID into the form so it gets submitted to the server
  var form = document.getElementById('payment-form');
  var hiddenInput = document.createElement('input');
  hiddenInput.setAttribute('type', 'hidden');
  hiddenInput.setAttribute('name', 'stripeToken');
  hiddenInput.setAttribute('value', token.id);
  form.appendChild(hiddenInput);

  // Submit the form
  form.submit();
}


})();

   	
  
</script>

 @endpush