@extends('layout')

@section('title','Login')

@section('content')

<section id="form"><!--form-->
		<div class="container">
			<div class="row">
				<div class="col-sm-3 col-sm-offset-1">
					<div class="login-form"><!--login form-->
						<h2>Login to your account</h2>
						<form action="{{url('/user_login')}}" method="POST">
							@csrf
							<input type="email" name="customer_email" placeholder="Email" autocomplete="off" />
							<input type="password" name="password" placeholder="Password" />
							
							<button type="submit" class="btn btn-default">Login</button>
						</form>
					</div><!--/login form-->
				</div>
				<div class="col-sm-1">
					<h2 class="or">OR</h2>
				</div>
				<div class="col-sm-4">
					<div class="signup-form"><!--sign up form-->
						<h2>New User Signup!</h2>
						<form action="{{URL::to('/user_registration')}}" method="POST">
							@csrf
							<input type="text" name="customer_name" placeholder="Full Name" autocomplete="off" />
							<input type="email" name="customer_email" placeholder="Email Address" autocomplete="off"/>
							<input type="password" name="Password" placeholder="Password"/>
							<input type="text" name="mobile_number" placeholder="Mobile Number" autocomplete="off"/>
							<input type="text" name="address" placeholder="Address" autocomplete="off"/>
							<input type="text" name="city" placeholder="City" autocomplete="off"/>
							<input type="text" name="post_code" placeholder="Post Code" autocomplete="off"/>
							<button type="submit" class="btn btn-default">Signup</button>
						</form>
					</div><!--/sign up form-->
				</div>
			</div>
		</div>
	</section>

@endsection
