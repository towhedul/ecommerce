@extends('layouts.app')

@section('title','Edit Blog')

@push('css')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="/resources/demos/style.css">

@endpush

@section('content')

<section class="content-header">
      <h1>
        Dashboard
        
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
        <li class="active"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit Blog</li>
      </ol>
</section>

<section class="content">
<div class="row">
        <div class="col-md-12">
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Blog</h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-toggle="dropdown">
                    <i class="fa fa-wrench"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
         <form class="form-horizontal" action="{{route('blog.update',$blog->id)}}" method="POST" enctype="multipart/form-data">
         	@csrf
          @method('PUT')

            <div class="box-body">
              <div class="row">
                 <div class="col-md-8">
                 	
                 		
                 		<div class="form-group">
		                  <label for="Blog Title" class="col-sm-4 control-label">Blog Title</label>

		                  <div class="col-sm-8">
		                    <input type="text" class="form-control" value="{{$blog->title}}" name="title">
		                  </div>
		                </div>

		                <div class="form-group">
		                  <label for="Blog Date" class="col-sm-4 control-label">Publish Date</label>

		                  <div class="col-sm-8">
		                    <input type="text" class="form-control" id="datepicker" name="date" value="{{date('d-m-y', strtotime($blog->date))}}">
		                  </div>
		                </div>

		                <div class="form-group">
		                  <label for="Blog Time" class="col-sm-4 control-label">Publish Time</label>

		                  <div class="col-sm-8">
		                    <input type="time" class="form-control" name="time" value="{{$blog->time}}">
		                  </div>
		                </div>

		                <div class="form-group hidden-phone">
		                  <label for="Short Description" class="col-sm-4 control-label">Short Description</label>

		                  <div class="col-sm-8">
		                      <textarea class="textarea" name="short_description" 
                                style="width: 100%; height: 80px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{$blog->short_description}}
                          	
                             </textarea>
		                  </div>
		                </div>

		                <div class="form-group hidden-phone">
		                  <label for="Long Description" class="col-sm-4 control-label">Long Description</label>

		                  <div class="col-sm-8">
		                      <textarea class="textarea" name="long_description" 
                                 style="width: 100%; height: 140px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{$blog->long_description}}
                          	
                             </textarea>
		                  </div>
		                </div>

                    <div class="form-group">
                      <label for="Image" class="col-sm-4 control-label">Old Product Image</label>

                      <div class="col-sm-8">
                        <img src="{{asset('upload/blog/'.$blog->blog_image)}}" alt="" style="height: 30%; width:30%">
                      </div>
                    </div>

		                <div class="form-group">
		                  <label for="Image" class="col-sm-4 control-label">New Product Image</label>

		                  <div class="col-sm-8">
		                    <input type="file" class="input-file uniform-on" id="fileInput" name="blog_image">
		                  </div>
		                </div>
                		
                 	
                 </div>
               
             </div>
              <!-- /.row -->
            </div>
            <!-- ./box-body -->
            <div class="box-footer">
              <div class="row">
                <div class="col-md-8">
                	<button type="submit" class="btn btn-primary pull-right">Submit
                	</button>

                	<button type="reset" class="btn btn-default"> Cancel
                	</button>
                	
                </div>
                            
              </div>
              <!-- /.row -->
            </div>
        </form> 
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>

  </section>  


@endsection

@push('scripts')

<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
$( function() {
    $( "#datepicker" ).datepicker();
  } );
  </script>


@endpush