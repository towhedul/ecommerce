@extends('layouts.app')
@section('title','All Blog')

@section('content')

<section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
        <li class="active"><i class="fa fa-slideshare" aria-hidden="true"> </i> All Blog</li>
      </ol>
</section> 

<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-success">
				<div class="box-header with-border">
					<h3 class="box-title">Blog List</h3>
				</div>
               
               

              @if(session('successMsg'))
                <div class="alert alert-success alert-dismissible" role="alert">
				  <strong>Success!</strong> - {{session('successMsg')}}
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
				    <span aria-hidden="true">&times;</span>
				  </button>
				</div>
              @endif



				<div class="box-body">
					<table id="example1" class="table table-bordered table-striped table-responsive">
						
						<thead>
							<tr>
								<th>Id</th>
								<th>Blog Title</th>
								<th>Image</th>
								<th>Date</th>
								<th>Time</th>
								<th>Short Description</th>
								<th>Long Description</th>
								<th>Status</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							<tr>
						@foreach($blogs as $key=>$blog)

                           
								<td>{{$key+1}}</td>
								<td>{{$blog->title}}</td>
								<td><img src="{{asset('upload/blog/'.$blog->blog_image)}}" style="width:100px;height:70px;"></td>
								<td>{{date("M d, Y", strtotime($blog->date))}}</td>
								<td>{{date("g:i A", strtotime($blog->time))}}</td>
								<td>{{$blog->short_description}}</td>
								<td>{{$blog->long_description}}</td>
								
								
								<td>
									@if($blog->publication_status==1)
									   <span class="label label-success">Active</span>
									@else 
									   <span class="label label-danger">Unctive</span>
									@endif
								</td>
								<td>
									@if($blog->publication_status==1)
									  <a href="" class="btn btn-danger btn-sm"><i class="fa fa-thumbs-down" aria-hidden="true"></i></a>
									@else 
									  <a href="" class="btn btn-success btn-sm"><i class="fa fa-thumbs-up" aria-hidden="true"></i></a>
									@endif

									<a href="{{route('blog.edit',$blog->id)}}" class="btn btn-info btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                     
                                    <form id="delete-form-{{$blog->id}}" action="{{route('blog.destroy',$blog->id)}}" method="POST" style="display:none">
                                    @csrf
                                    @method('DELETE')	
                                    </form>

									<button class="btn btn-danger btn-sm" onclick="if(confirm('Are you sure you want to delete this slider permanently !!')){
										event.preventDefault();
										document.getElementById('delete-form-{{$blog->id}}').submit();
									}else{
										event.preventDefault();
									}"><i class="fa fa-trash" aria-hidden="true"></i></button>
								</td>
							
							</tr>
							@endforeach	
						</tbody>
					</table>
				</div>

			</div>
		</div>
	</div>
	
</section>

@endsection