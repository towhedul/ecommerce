@extends('layouts.app')

@section('title','Update category')

@section('content')

<section class="content-header">
      <h1>
        Dashboard
        
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
        <li class="active"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Update Category</li>
      </ol>
</section> 

<section class="content">
<div class="row">
        <div class="col-md-12">
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">Add New Category</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
               
                <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-wrench"></i></button>
                              
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
         <form class="form-horizontal" action="{{route('brand.update',$brands->brand_id)}}" method="POST">
         	@csrf
         	@method('put')

            <div class="box-body">
              <div class="row">
                 <div class="col-md-8">
                 	
                 		
                 		<div class="form-group">
		                  <label for="Brand Name" class="col-sm-4 control-label">Brand Name</label>

		                  <div class="col-sm-8">
		                    <input type="text" class="form-control" id="brand_name"  name="brand_name" value="{{$brands->brand_name}}">
		                  </div>
		                </div>

		                <div class="form-group hidden-phone">
		                  <label for="Brand Description" class="col-sm-4 control-label">Brand Description</label>

		                  <div class="col-sm-8">
		                      <textarea class="textarea" name="brand_description"
                          style="width: 100%; height: 140px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{$brands->brand_description}}

                          	    
                             </textarea>
		                  </div>
		                </div>		                
                	
                 </div>
               
             </div>
              <!-- /.row -->
            </div>
            <!-- ./box-body -->
            <div class="box-footer">
              <div class="row">
                <div class="col-md-8">
                	<button type="submit" class="btn btn-primary pull-right">Update
                	</button>

                	<button type="reset" class="btn btn-default"> Cancel
                	</button>
                	
                </div>
                            
              </div>
              <!-- /.row -->
            </div>
        </form> 
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>

  </section>  

@endsection

