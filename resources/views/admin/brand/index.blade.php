@extends('layouts.app')

@section('title','Brand')

@section('content')

<section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
        <li class="active"><i class="fa fa-list"> </i> All Brand</li>
      </ol>
</section>  

<section class="content">
      <div class="row">
        <div class="col-xs-12">
          
            <!-- /.box-header -->           
          <div class="box box-success">
            <div class="box-header">
              <h3 class="box-title">Brand List</h3>
            </div>

         @if(session('successMsg'))

         <div class="alert alert-success alert-dismissible" role="alert">                   
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
          </button>
            <span>Success - {{session('successMsg')}}</span>
         </div> 
         @endif

            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped table-responsive">
                <thead>
                <tr>
                  <th>Brand ID</th>
                  <th>Brand Name</th>
                  <th>Brand Description</th>
                  <th>Status</th>
                  <th class="pull-right">Action</th>
                </tr>
                </thead>
                <tbody>
               @foreach($brands as $brand) 	
                <tr>
                  <td>{{$brand->brand_id}}</td>
                  <td>{{$brand->brand_name}}</td>
                  <td>{{$brand->brand_description}}</td>
                  <td>

                  @if($brand->publication_status==1)
                  <span class="label label-success">Active</span>
                  @else
                  <span class="label label-danger">Unactive</span>
                  @endif
                  </td>
                  <td class="pull-right">
                   
                   @if($brand->publication_status==1)
                  	<a class="btn btn-danger btn-sm" href="{{URL::to('/unactive_brand/'.$brand->brand_id)}}"><i class="fa fa-thumbs-down" aria-hidden="true"></i>
                  	</a>
                  	@else
                  	<a class="btn btn-success btn-sm" href="{{URL::to('/active_brand/'.$brand->brand_id)}}"><i class="fa fa-thumbs-up" aria-hidden="true"></i>
                  	</a>
                  	@endif

                  	<a class="btn btn-info btn-sm" href="{{route('brand.edit',$brand->brand_id)}}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                  	</a>

                    <form id="delete-form-{{$brand->brand_id}}" action="{{route('brand.destroy',$brand->brand_id)}}" method="POST" style="display:none">
                      @csrf
                      @method('DELETE')
                      
                    </form>

                  	<button class="btn btn-danger btn-sm"
                    onclick="if(confirm('Are You Sure You Want to Delete This')){
                        event.preventDefault();
                         document.getElementById('delete-form-{{$brand->brand_id}}').submit();
                    }else{
                           event.preventDefault();
                    }
                    ">
                    <i class="fa fa-trash" aria-hidden="true"></i>
                    </button>
                  </td>
                </tr> 
                @endforeach                             
                </tbody>
               
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>

@endsection


@push('scripts')

<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>

@endpush