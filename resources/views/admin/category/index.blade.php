@extends('layouts.app')

@section('title','Category')

@section('content')

<section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
        <li class="active"><i class="fa fa-list"> </i> All Category</li>
      </ol>
</section>  

<section class="content">
      <div class="row">
        <div class="col-xs-12">
          
            <!-- /.box-header -->           
          <div class="box box-success">
            <div class="box-header">
              <h3 class="box-title">Category List</h3>
            </div>

         @if(session('successMsg'))

         <div class="alert alert-success alert-dismissible" role="alert">                   
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
          </button>
            <span>Success - {{session('successMsg')}}</span>
         </div> 
         @endif


            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped table-responsive">
                <thead>
                <tr>
                  <th>Category ID</th>
                  <th>Category Name</th>
                  <th>Category Description</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
               @foreach($categories as $key=>$category) 	
                <tr>
                  <td>{{$category->category_id}}</td>
                  <td>{{$category->category_name}}</td>
                  <td>{{$category->category_description}}</td>
                  <td>

                  @if($category->publication_status==1)
                  <span class="label label-success">Active</span>
                  @else
                  <span class="label label-danger">Unactive</span>
                  @endif
                  </td>
                  <td>
                   
                   @if($category->publication_status==1)
                  	<a class="btn btn-danger btn-sm" href="{{URL::to('/unactive/'.$category->category_id)}}"><i class="fa fa-thumbs-down" aria-hidden="true"></i>
                  	</a>
                  	@else
                  	<a class="btn btn-success btn-sm" href="{{URL::to('/active/'.$category->category_id)}}"><i class="fa fa-thumbs-up" aria-hidden="true"></i>
                  	</a>
                  	@endif

                  	<a class="btn btn-info btn-sm" href="{{route('category.edit',$category->category_id)}}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                  	</a>

                    <form id="delete-form-{{$category->category_id}}" action="{{route('category.destroy',$category->category_id)}}" method="POST" style="display:none">
                      @csrf
                      @method('DELETE')
                      
                    </form>

                  	<button class="btn btn-danger btn-sm"
                    onclick="if(confirm('Are You Sure You Want to Delete This')){
                        event.preventDefault();
                         document.getElementById('delete-form-{{$category->category_id}}').submit();
                    }else{
                           event.preventDefault();
                    }
                    ">
                    <i class="fa fa-trash" aria-hidden="true"></i>
                    </button>
                  </td>
                </tr> 
                @endforeach                             
                </tbody>
               
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>

@endsection


@push('scripts')

<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>

@endpush


