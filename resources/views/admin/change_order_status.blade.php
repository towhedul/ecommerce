@extends('layouts.app')
@section('title','Change Order Status')

@section('content')

<section class="content-header">
      <h1>Dashboard<small>Control panel</small></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i><strong> Home</strong></a></li>
        <li class="active"><i class="fa fa-user-circle-o" aria-hidden="true"> </i><strong> Change Status</strong></li>
      </ol>
</section>

<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-success">
				<div class="box-header with-border">
					<h3 class="box-title"><i class="fa fa-list" aria-hidden="true"> </i> Change Order Status</h3>
				</div>              

		<form class="form-horizontal" action="{{route('manage_order.update',$orders->order_id)}}" method="POST">
         @csrf
         @method('PUT')

            <div class="box-body">
              <div class="row">
                 <div class="col-md-8">
                                  		
             		<div class="form-group">
	                  <label for="Order Status" class="col-sm-4 control-label">Order Status</label>

	                  <div class="col-sm-8">
	                    
		                    <select class="form-control" id="sel1" name="status">
		                    	<option value="{{$orders->status}}">  
                                    @if($orders->status == 0)
                                 <span class="label label-warning">Pending</span>
                                    @elseif($orders->status == 1)
                                 <span class="label label-info">Processing</span>
                                    @else
                                 <span class="label label-danger">Delivered</span>
                                    @endif

		                    	</option>
							    <option value="0"> Pending </option>
							    <option value="1"> Processing</option>
							    <option value="2"> Delivered</option>
							    
							</select>
	                  </div>
	                </div>
	                                 	
                 </div>
               
             </div>
              <!-- /.row -->
            </div>
            <!-- ./box-body -->
            <div class="box-footer">
              <div class="row">
                <div class="col-md-8">
                	<button type="submit" class="btn btn-primary pull-right">Submit
                	</button>

                	<button type="reset" class="btn btn-default"> Cancel
                	</button>
                	
                </div>
                            
              </div>
              <!-- /.row -->
            </div>
        </form> 

			</div>
		</div>

		
	</div>
	
</section>

@endsection