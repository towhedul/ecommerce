@extends('layouts.app')
@section('title','Manage Order')

@section('content')

<section class="content-header">
      <h1>Dashboard<small>Control panel</small></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i><strong> Home</strong></a></li>
        <li class="active"><i class="fa fa-user-circle-o" aria-hidden="true"> </i><strong> Manage Order</strong></li>
      </ol>
</section> 

<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-success">
				<div class="box-header with-border">
					<h3 class="box-title"><i class="fa fa-list" aria-hidden="true"> </i>    
					Orders List</h3>
				</div>              

              @if(session('successMsg'))
                <div class="alert alert-success alert-dismissible" role="alert">
				  <strong>Success!</strong> - {{session('successMsg')}}
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
				    <span aria-hidden="true">&times;</span>
				  </button>
				</div>
              @endif


				<div class="box-body">
					<table id="example1" class="table table-bordered table-striped table-responsive">
						
						<thead>
							<tr>
								<th>Order Id</th>
								<th>Customer Name</th>
								<th>Order Total</th>
								<th>Order Status</th>
								<th>Payment Id</th>
								<th>Transaction Id</th>
								<th>Payment Status</th>
								<th>Invoice</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
						 @foreach($orders as $order)
						    <tr>
						    	<td>{{$order->order_id}}</td>
						    	<td>{{$order->customer_name}}</td>
						    	<td>{{$order->total_amount}}</td>
						    	<td>
						    		@if($order->status==0)
									   <span class="label label-warning">Pending</span>
									@elseif($order->status==1)
									   <span class="label label-info">Processing</span>
									@else 
									   <span class="label label-danger">Delivered</span>
									@endif
						    	</td>
						    	<td>{{$order->payment_id}}</td>
						    	<td>{{$order->transaction_id}}</td>
						    	<td><span class="label label-success">{{$order->payment_status}}
						    	    </span>
						    	</td>
						    	<td>
						    		<a href="{{URL::to('/admin/invoice/'.$order->order_id)}}" class="btn btn-info btn-sm" target="blank">Invoice</a>
						    	</td>
						    	<td>
						    		
								   <a href="{{URL::to('/show_order_details/'.$order->order_id)}}" class="btn btn-warning btn-sm" title="Show Details"><i class="fa fa-eye" aria-hidden="true"></i></a>
									
									 
									<a href="{{route('manage_order.edit',$order->order_id)}}" class="btn btn-info btn-sm" title="Edit"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>

									<form id="delete-form-{{$order->order_id}}" action="{{route('manage_order.destroy',$order->order_id)}}" method="POST" style="display:none">
                                    @csrf
                                    @method('DELETE')	
                                    </form>

									<button class="btn btn-danger btn-sm" title="Delete" onclick="if(confirm('Are you sure you want to delete this slider permanently !!')){
										event.preventDefault();
										document.getElementById('delete-form-{{$order->order_id}}').submit();
									}else{
										event.preventDefault();
									}"><i class="fa fa-trash" aria-hidden="true"></i></button>

						    	</td>
						    </tr>
                         @endforeach
						
							
						
						</tbody>
						
					</table>
				</div>

			</div>
		</div>
	</div>
	
</section>

@endsection

@push('scripts')

<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>

@endpush

