@extends('layouts.app')
@section('title','Manage Payment')

@section('content')

<section class="content-header">
      <h1>Dashboard<small>Control panel</small></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i><strong> Home</strong></a></li>
        <li class="active"><i class="fa fa-rocket" aria-hidden="true"></i><strong> Manage Payment
        </strong></li>
      </ol>
</section> 

<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-success">
				<div class="box-header with-border">
					<h3 class="box-title"><i class="fa fa-list" aria-hidden="true"> </i>    
					Orders List</h3>
				</div>              

              @if(session('successMsg'))
                <div class="alert alert-success alert-dismissible" role="alert">
				  <strong>Success!</strong> - {{session('successMsg')}}
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
				    <span aria-hidden="true">&times;</span>
				  </button>
				</div>
              @endif


				<div class="box-body">
					<table id="example1" class="table table-bordered table-striped table-responsive">
						
						<thead>
							<tr>
								<th>Payment Id</th>
								<th>Transaction Id</th>
								<th>Payment Type</th>
								<th>Payment Method</th>
								<th>Currency</th>
								<th>Total Amount</th>
								<th>Payment Status</th>
								<th>Recipt</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
					@foreach($payments as $payment)	 
						    <tr>
						    	<td>{{$payment->payment_id}}</td>
						    	<td>{{$payment->transaction_id}}</td>
						    	<td>{{$payment->payment_type}}<br>
                                    ****{{$payment->card_number}}
						    	</td>
						    	<td>{{$payment->payment_method}}</td>
						    	<td>{{$payment->currency}}</td>
						    	<td>{{$payment->amount}}</td>
						    	<td><span class="label label-success">{{$payment->payment_status}}</span></td>
						    	<th>
						    		<a href="{{$payment->receipt_url}}" target="_blank">Print</a>
						    	</th>
						    	<td>
						    		
								   <a href=""  class="btn btn-warning btn-sm " title="Change Status"><i class="fa fa-eye" aria-hidden="true"></i></a>                                   
										 
									<!-- <a href="" class="btn btn-info btn-sm" title="Change Status"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
									 -->
									<form id="delete-form-{{$payment->payment_id}}" action="{{route('manage_payment.destroy',$payment->payment_id)}}" method="POST" style="display:none">
                                    @csrf
                                    @method('DELETE')	
                                    </form>

									<button class="btn btn-danger btn-sm" title="Delete" onclick="if(confirm('Are you sure you want to delete this slider permanently !!')){
										event.preventDefault();
										document.getElementById('delete-form-{{$payment->payment_id}}').submit();
									}else{
										event.preventDefault();
									}"><i class="fa fa-trash" aria-hidden="true"></i></button>

						    	</td>
						    </tr>
                    @endforeach  
												
						</tbody>
						
					</table>
				</div>

			</div>
		</div>
	</div>

</section>


@endsection

@push('scripts')

<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>


<!-- <script>
	
	$(document).ready(function(){

		var table=$('#dataTable').DatatTable();

		table.on('click','.edit',function()){

			$tr = $(this).closest('tr');
			if($($tr).hasClass('child')){
				$tr = $tr.prev('.prent');
			}

			var data=table.row($tr).data();
			console.log(data);

			$('#').val(data[1]);
			$('#').val(data[2]);
			$('#').val(data[3]);
			$('#').val(data[4]);

		$('#editForm').attr('action','/manage_payment/'+data[0]);
		$('#exampleModalLong').modal('show');

		}
	});
</script>
 -->
@endpush
