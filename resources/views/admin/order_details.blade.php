@extends('layouts.app')
@section('title','Oreders Details')

@section('content')

<section class="content-header">
      <h1>Dashboard<small>Control panel</small></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i><strong> Home</strong></a></li>
        <li class="active"><i class="fa fa-user-circle-o" aria-hidden="true"> </i><strong> Orders Details</strong></li>
      </ol>
</section>


<section class="content">
	<div class="row">

		<div class="col-md-6">
			<div class="box box-success">
				<div class="box-header with-border">
					<h3 class="box-title"><i class="fa fa-user" aria-hidden="true"></i>   
					Customer Details</h3>
				</div>              

				<div class="box-body">
					<table class="table table-hover table-striped table-responsive">
						
						<thead>
							<tr>
								<th>Customer Id</th>
								<th>Customer Name</th>
								<th>Customer Details</th>
															
							</tr>
						</thead>
						<tbody>
						    <tr>    
						        <td>Customer Id</td>
						        <td>Customer Name</td>
								<td>
                                    <p>Customer Details</p>
                                    <p>Customer Details</p>
                                    <p>Customer Details</p>
                                    <p>Customer Details</p>
                                    <p>Customer Details</p>
								</td>
								
						    </tr>
						</tbody>
						
					</table>
				</div>

			</div>
		</div>


		<div class="col-md-6">
			<div class="box box-success">
				<div class="box-header with-border">
					<h3 class="box-title"><i class="fa fa-list" aria-hidden="true"> </i>    
					Shipping Details</h3>
				</div>              

				<div class="box-body">
					<table class="table table-hover table-striped table-responsive">
						
						<thead>
							<tr>
								<th>Shipping Id</th>
								<th>Username</th>
								<th>Recipient Name</th>
								<th>Shipping Details</th>								
							</tr>
						</thead>
						<tbody>
						    <tr>
								<th>Shipping Id</th>
								<th>Username</th>
								<th>Recipient Name</th>
								<th>
									<p>Address</p>
									<p>Address</p>
									<p>Address</p>
									<p>Address</p>
								</th>								
							</tr>
						
						</tbody>
						
					</table>
				</div>

			</div>
		</div>


	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="box box-success">
				<div class="box-header with-border">
					<h3 class="box-title"><i class="fa fa-list" aria-hidden="true"> </i>    
					Order Details</h3>
				</div>              

				<div class="box-body">
					<table class="table table-hover table-striped table-responsive">
						
						<thead>
							<tr>
								<th>Order Id</th>
								<th>Product Name</th>
								<th>Product Price</th>
								<th>Product Quantity</th>								
								<th>Product Price</th>								
							</tr>
						</thead>
						<tbody>
						@foreach($order_details as $details)
						    <tr>
								<td>{{$details->product_name}}</td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
																
							</tr>
					@endforeach
						</tbody>
						
					</table>
				</div>

			</div>
		</div>

		
	</div>
	
</section>

@endsection