@extends('layouts.app')

@section('title','Add New Products')

@section('content')

<section class="content-header">
      <h1>
        Dashboard
        
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
        <li class="active"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Add New Products</li>
      </ol>
</section>

<section class="content">
<div class="row">
        <div class="col-md-12">
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">Add New Sub Category</h3>
              @if($errors->any())
                  @foreach($errors->all() as $error)
                     <div class="alert alert-danger alert-dismissible" role="alert">
                      <strong>Danger!</strong> - {{$error}}
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                  @endforeach
              @endif

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
               
                <button type="button" class="btn btn-box-tool" data-toggle="dropdown">
                    <i class="fa fa-wrench"></i></button>
                              
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
         <form class="form-horizontal" action="{{route('products.store')}}" method="POST" enctype="multipart/form-data">
         	@csrf

            <div class="box-body">
              <div class="row">
                 <div class="col-md-8">

                 	<div class="form-group">
	                  <label for="Product Name" class="col-sm-4 control-label">Product Name</label>

	                  <div class="col-sm-8">
	                    <input type="text" class="form-control" id="product_name" placeholder="Enter Product Name" name="product_name">
	                  </div>
	                </div>


             	    <div class="form-group">
	                  <label for="Category Name" class="col-sm-4 control-label"> Category Name</label>
	                  <div class="col-sm-8">                    
	                   <select class="form-control" id="sel1" name="category_id">
							<option>---Select Category Here---</option>
					<?php 
                        $categories=DB::table('tbl_category')
                                   ->where('publication_status',1)
                                   ->get();
                       foreach($categories as $key=>$category){            
					?>		
							<option value="{{$category->category_id}}">{{$category->category_name}}</option>
					<?php } ?>	
						</select>                    
	                  </div>
	                </div>

	                <div class="form-group">
	                  <label for="Subcategory Name" class="col-sm-4 control-label"> Subcategory Name</label>
	                  <div class="col-sm-8">                    
	                   <select class="form-control" id="sel1" name="sub_category_id">
							<option>---Select Subcategory Here---</option>
					<?php
                        $sub_categories=DB::table('tbl_sub_category')
                                       ->where('publication_status',1)
                                       ->get();
                        foreach($sub_categories as $key=>$sub_category){               
					?>		
							<option value="{{$sub_category->sub_category_id}}">{{$sub_category->sub_category_name}}</option>
					<?php } ?>	
						</select>                    
	                  </div>
	                </div>

	                <div class="form-group">
	                  <label for="Brand Name" class="col-sm-4 control-label"> Brand Name</label>
	                  <div class="col-sm-8">                    
	                   <select class="form-control" name="brand_name">
							<option>---Select Any Brand If Necessary---</option>
					<?php 
                        $brands=DB::table('tbl_brand')
                                   ->where('publication_status',1)
                                   ->get();
                       foreach($brands as $key=>$brand){            
					?>		
							<option value="{{$brand->brand_name}}">{{$brand->brand_name}}</option>
					<?php } ?>	
						</select>                    
	                  </div>
	                </div>
                 		
                 	  
	                <div class="form-group hidden-phone">
	                  <label for="Product Short Description" class="col-sm-4 control-label">Short Description</label>

	                  <div class="col-sm-8">
	                      <textarea class="textarea" name="product_short_description" 
                      style="width: 100%; height: 140px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">
                      	
                         </textarea>
	                  </div>
	                </div>


	                <div class="form-group hidden-phone">
	                  <label for="Product Long Description" class="col-sm-4 control-label">Long Description</label>

	                  <div class="col-sm-8">
	                      <textarea class="textarea" name="product_long_description" 
                      style="width: 100%; height: 140px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">
                      	
                         </textarea>
	                  </div>
	                </div>

	                
	                <div class="form-group">
	                  <label for="Product Price" class="col-sm-4 control-label">Product Price</label>

	                  <div class="col-sm-8">
	                    <input type="text" class="form-control" id="product_price" placeholder="Enter Product Price" name="product_price">
	                  </div>
	                </div>

	                <div class="form-group">
	                  <label for="Product Image" class="col-sm-4 control-label">Product Image</label>

	                  <div class="col-sm-8">
	                    <input type="file" class="input-file uniform-on" id="fileInput" name="product_image">
	                  </div>
	                </div>

	                <div class="form-group">
	                  <label for="Product Size" class="col-sm-4 control-label">Product Size</label>

	                  <div class="col-sm-8">
	                    <input type="text" class="form-control" id="product_size" placeholder="Enter Product Size" name="product_size">
	                  </div>
	                </div>

	                <div class="form-group">
	                  <label for="Product Color" class="col-sm-4 control-label">Product Color</label>

	                  <div class="col-sm-8">
	                    <input type="text" class="form-control" id="product_color" placeholder="Enter Product Color" name="product_color">
	                  </div>
	                </div>

	                <div class="form-group">
	                  <label for="Product Quantity" class="col-sm-4 control-label">Product Quantity</label>

	                  <div class="col-sm-8">
	                    <input type="text" class="form-control" id="product_quenty" placeholder="Enter Product Quantity" name="product_quantity">
	                  </div>
	                </div>

	                <div class="form-group">
	                  <label for="publication_status" class="col-sm-4 control-label">Publication Status</label>

	                  <div class="col-sm-8">
	                    <input type="checkbox" name="publication_status" value="1">
	                  </div>
	                </div>
                 		
                 	
                 </div>
               
             </div>
              <!-- /.row -->
            </div>
            <!-- ./box-body -->
            <div class="box-footer">
              <div class="row">
                <div class="col-md-8">
                	<button type="submit" class="btn btn-primary pull-right">Submit
                	</button>

                	<button type="reset" class="btn btn-default"> Cancel
                	</button>
                	
                </div>
                            
              </div>
              <!-- /.row -->
            </div>
        </form> 
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>

  </section> 

@endsection



