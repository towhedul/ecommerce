@extends('layouts.app')

@section('title','All Products')

@section('content')

<section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
        <li class="active"><i class="fa fa-list"> </i> All Products</li>
      </ol>
</section> 

<section class="content">
      <div class="row">
        <div class="col-md-12">
          
            <!-- /.box-header -->           
          <div class="box box-success">
            <div class="box-header">
              <h3 class="box-title">Product List</h3>
            </div>

            @if(session('successMsg'))

             <div class="alert alert-success alert-dismissible" role="alert">                   
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
              </button>
                <span>Success - {{session('successMsg')}}</span>
             </div> 
           @endif

            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped table-responsive">
                <thead>
                <tr>
                  <th>ID</th>
                  <th>Name</th>
                  <th>Image</th>
                  <th>Size</th>
                  <th>Price</th>
                  <th>Color</th>
                  <th>Quantity</th>
                  <th>Category Name</th>
                  <th>Subcategory Name</th>
                  <th>Brand</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>

                <tbody>
                @foreach($products as $key=>$product)	
                	<tr>
                		<td>{{$key+1}}</td>
                		<td>{{$product->product_name}}</td>
                		<td><img src="{{URL::to($product->product_image)}}" style="width:90px;height:100px;"></td>
                		<td>{{$product->product_size}}</td>
                		<td>{{$product->product_price}}</td>
                		<td>{{$product->product_color}}</td>
                		<td>{{$product->product_quantity}}</td>
                		<td>{{$product->category_name}}</td>
                		<td>{{$product->sub_category_name}}</td>
                    <td>{{$product->brand_name}}</td>
                		<td>
                			@if($product->publication_status==1)
                			  <span class="label label-success">Active</span>
                			@else 
                			  <span class="label label-danger">Unactive</span>
                			@endif
                		</td>
                		<td>
                			@if($product->publication_status==1)
                			<a href="{{URL::to('/active_product/'.$product->product_id)}}" class="btn btn-danger btn-sm"><i class="fa fa-thumbs-down" aria-hidden="true"></i></a>
                			@else
                      <a href="{{URL::to('/unactive_product/'.$product->product_id)}}" class="btn btn-success btn-sm"><i class="fa fa-thumbs-up" aria-hidden="true"></i></a>
                      @endif

                			<a href="{{route('products.edit',$product->product_id)}}" class="btn btn-info btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>

                			<form id="delete-form-{{$product->product_id}}" action="{{route('products.destroy',$product->product_id)}}" method="POST" style="display:none">
                        @csrf
                        @method('DELETE')
                      </form>
                      <button class="btn btn-danger btn-sm" onclick="if(confirm('Are you sure you want ot delete this permanently ! ')){
                        event.preventDefault();
                        document.getElementById('delete-form-{{$product->product_id}}').submit();

                      }else{
                          event.preventDefault();
                        }
                      "><i class="fa fa-trash" aria-hidden="true"></i></button>
                		</td>
                	</tr>
                @endforeach	
                </tbody>

                             
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>

@endsection

@push('scripts')

<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>

@endpush