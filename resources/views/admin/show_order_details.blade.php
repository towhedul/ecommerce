@extends('layouts.app')
@section('title','Show Order Details')

@section('content')

<section class="content">
	<div class="row">

		<div class="col-md-6">
			<div class="box box-success">
				<div class="box-header with-border">
					<h3 class="box-title"><i class="fa fa-user" aria-hidden="true"></i>   
					Customer Details</h3>
				</div>              

				<div class="box-body">
					<table class="table table-hover table-striped table-responsive">
						
						<thead>
							@foreach($order_details as $order)
							@endforeach
							<tr>
								<th>Customer Id</th>
								<th>Customer Name</th>
								<th>Customer Details</th>
															
							</tr>
						</thead>
						<tbody>
						
						     <tr>    
						        <th>{{$order->customer_id}}</th>
						        <th>{{$order->customer_name}}</th>
								<th>
                                    <p>{{$order->customer_email}}</p>
                                    <p>{{$order->mobile_number}}</p>
                                    <p>{{$order->address}}</p>
                                    <p>{{$order->city}}</p>
                                    <p>{{$order->post_code}}</p>
								</th>
								
						    </tr>
						    					
						</tbody>
						
					</table>
				</div>

			</div>
		</div>


		<div class="col-md-6">
			<div class="box box-success">
				<div class="box-header with-border">
					<h3 class="box-title"><i class="fa fa-list" aria-hidden="true"> </i>    
					Shipping Details</h3>
				</div>              

				<div class="box-body">
					<table class="table table-hover table-striped table-responsive">
						
						<thead>

							<tr>
								<th>Shipping Id</th>
								<th>Username</th>
								<th>Recipient Name</th>
								<th>Shipping Details</th>								
							</tr>
						</thead>
						<tbody>
						     <tr>
								<th>{{$order->shipping_id}}</th>
								<th>{{$order->shipping_email}}</th>
								<th>{{$order->shipping_first_name}} {{$order->shipping_last_name}}</th>
								<th>
									<p>{{$order->shipping_address}}</p>
									<p>{{$order->shipping_mobile_number}}</p>
									<p>{{$order->shipping_city}}</p>
									
								</th>								
							</tr>
						 
						</tbody>
						
					</table>
				</div>

			</div>
		</div>


	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="box box-success">
				<div class="box-header with-border">
					<h3 class="box-title"><i class="fa fa-list" aria-hidden="true"> </i>    
					Order Details</h3>
				</div>              

				<div class="box-body">
					<table class="table table-hover table-striped table-responsive">
						
						<thead>
							<tr>
								<th>Order Id</th>
								<th>Order Date</th>
								<th>Product Name</th>
								<th>Product Image</th>
								
								<th>Product Price</th>
								<th>Product Quantity</th>								
								<th>Product Price</th>								
							</tr>
						</thead>
						<tbody>
						@foreach($order_details as $order)
						    <tr>
								<td>{{$order->order_id}}</td>
								<td>{{$order->order_date}}</td>
								<td>{{$order->product_name}}</td>
								<td><img src="{{URL::to($order->product_image)}}" style="width:90px;height:60px"></td>
								<td>{{$order->price}}</td>
								<td>{{$order->quantity}}</td>
								<td>{{$order->price*$order->quantity}}</td>
																
							</tr>
						    
					    @endforeach
					        <tr>
					        	<th colspan="6">Total Amount (Including Tax)</th>
					            <th >{{$order->total_amount}}</th>
					        </tr>
						</tbody>
						
					</table>
				</div>

			</div>
		</div>

		
	</div>
	
</section>


@endsection