<div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="width: 960px;margin-left: -200px;">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle" style="float: left;">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
         
       <section class="content">
	<div class="row">

		<div class="col-md-6">
			<div class="box box-success">
				<div class="box-header with-border">
					<h3 class="box-title"><i class="fa fa-user" aria-hidden="true"></i>   
					Customer Details</h3>
				</div>              

				<div class="box-body">
					<table class="table table-hover table-striped table-responsive">
						
						<thead>
							<tr>
								<th>Customer Id</th>
								<th>Customer Name</th>
								<th>Customer Details</th>
															
							</tr>
						</thead>
						<tbody>
						    <tr>    
						        <td>Customer Id</td>
						        <td>Customer Name</td>
								<td>
                                    <p>Customer Details</p>
                                    <p>Customer Details</p>
                                    <p>Customer Details</p>
                                    <p>Customer Details</p>
                                    <p>Customer Details</p>
								</td>
								
						    </tr>
						</tbody>
						
					</table>
				</div>

			</div>
		</div>


		<div class="col-md-6">
			<div class="box box-success">
				<div class="box-header with-border">
					<h3 class="box-title"><i class="fa fa-list" aria-hidden="true"> </i>    
					Shipping Details</h3>
				</div>              

				<div class="box-body">
					<table class="table table-hover table-striped table-responsive">
						
						<thead>
							<tr>
								<th>Shipping Id</th>
								<th>Username</th>
								<th>Recipient Name</th>
								<th>Shipping Details</th>								
							</tr>
						</thead>
						<tbody>
						    <tr>
								<th>Shipping Id</th>
								<th>Username</th>
								<th>Recipient Name</th>
								<th>
									<p>Address</p>
									<p>Address</p>
									<p>Address</p>
									<p>Address</p>
								</th>								
							</tr>
						
						</tbody>
						
					</table>
				</div>

			</div>
		</div>


	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="box box-success">
				<div class="box-header with-border">
					<h3 class="box-title"><i class="fa fa-list" aria-hidden="true"> </i>    
					Order Details</h3>
				</div>              

				<div class="box-body">
					<table class="table table-hover table-striped table-responsive">
						
						<thead>
							<tr>
								<th>Order Id</th>
								<th>Product Name</th>
								<th>Product Price</th>
								<th>Product Quantity</th>								
								<th>Product Price</th>								
							</tr>
						</thead>
						<tbody>
						
						    <tr>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
																
							</tr>
					
						</tbody>
						
					</table>
				</div>

			</div>
		</div>

		
	</div>
	
</section>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>        
      </div>
    </div>
  </div>
</div>