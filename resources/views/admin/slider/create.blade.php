@extends('layouts.app')
@section('title','Add Slider')

@section('content')

<section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
        <li class="active"><i class="fa fa-slideshare" aria-hidden="true"> </i> All Slider</li>
      </ol>
</section> 

<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-success">
				<div class="box-header with-border">
	              <h3 class="box-title">Add Slider</h3>
	              @if($errors->any())
                     @foreach($errors->all() as $error)
                     <div class="alert alert-danger alert-dismissible" role="alert">
					  <strong>Danger!</strong> - {{$error}}
					  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
					    <span aria-hidden="true">&times;</span>
					  </button>
					</div>

                     @endforeach
               @endif


	              <div class="box-tools pull-right">
	                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
	                </button>
	               
	                <button type="button" class="btn btn-box-tool" data-toggle="dropdown">
	                    <i class="fa fa-wrench"></i></button>
	                              
	                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
	              </div>
	            </div>
            <!-- /.box-header -->
            <form class="form-horizontal" action="{{route('slider.store')}}" method="POST" enctype="multipart/form-data">
            	@csrf
            	<div class="box-body">
            		<div class="row">
            			<div class="col-md-8">

            				<div class="form-group">
			                  <label for="Image Title" class="col-sm-4 control-label">Image Title</label>

			                  <div class="col-sm-8">
			                    <input type="text" class="form-control" id="image_title" placeholder="Enter Image Title" name="image_title">
			                  </div>
			                </div>

			                <div class="form-group hidden-phone">
			                  <label for="Image Subtitle" class="col-sm-4 control-label">Image Subtitle</label>

			                  <div class="col-sm-8">
			                      <textarea class="textarea" name="image_subtitle" 
		                      style="width: 100%; height: 90px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">
		                      	
		                         </textarea>
			                  </div>
			                </div>

			                <div class="form-group">
			                  <label for="Slider Image" class="col-sm-4 control-label">Slider Image</label>

			                  <div class="col-sm-8">
			                    <input type="file" class="input-file uniform-on form-control" id="fileInput" name="slider_image">
			                  </div>
			                </div>

			                <div class="form-group">
			                  <label for="publication_status" class="col-sm-4 control-label">Publication Status</label>

			                  <div class="col-sm-8">
			                    <input type="checkbox" name="publication_status" value="1">
			                  </div>
			                </div>
            				
            			</div>
            		</div>
            	</div>
            	<div class="box-footer">
            		<div class="row">
            			<div class="col-md-8">
            				<button type="submit" class="btn btn-primary pull-right">  Submit
                	       </button>

		                	<button type="reset" class="btn btn-default">Cancel
		                	</button>
            			</div>
            		</div>
            	</div>
            </form>
			</div>
		</div>
	</div>
</section>

@endsection