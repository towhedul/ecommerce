@extends('layouts.app')

@section('title','Edit Slider')

@section('content')

<section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
        <li class="active"><i class="fa fa-slideshare" aria-hidden="true"> </i> Edit Slider</li>
      </ol>
</section> 

<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-success">
				<div class="box-header with-border">
	              <h3 class="box-title">Edit Slider</h3>

	              <div class="box-tools pull-right">
	                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
	                </button>
	               
	                <button type="button" class="btn btn-box-tool" data-toggle="dropdown">
	                    <i class="fa fa-wrench"></i></button>
	                              
	                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
	              </div>
	            </div>
            <!-- /.box-header -->
            <form class="form-horizontal" action="{{route('slider.update',$slider->id)}}" method="POST" enctype="multipart/form-data">
            	@csrf
            	@method('put')
            	<div class="box-body">
            		<div class="row">
            			<div class="col-md-8">

            				<div class="form-group">
			                  <label for="Image Title" class="col-sm-4 control-label">Image Title</label>

			                  <div class="col-sm-8">
			                    <input type="text" class="form-control" id="image_title"  name="image_title" value="{{$slider->image_title}}">
			                  </div>
			                </div>

			                <div class="form-group hidden-phone">
			                  <label for="Image Subtitle" class="col-sm-4 control-label">Image Subtitle</label>

			                  <div class="col-sm-8">
			                      <textarea class="textarea" name="image_subtitle" 
		                      style="width: 100%; height: 90px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{$slider->image_subtitle}}
		                      	
		                         </textarea>
			                  </div>
			                </div>

			                <div class="form-group">
			                  <label for="Slider Image" class="col-sm-4 control-label">Slider Old Image</label>

			                  <div class="col-sm-8">
			                    <img src="{{asset('upload/slider/'.$slider->slider_image)}}" style="width:100%;height:150px;">
			                  </div>
			                </div>

			                <div class="form-group">
			                  <label for="Slider Image" class="col-sm-4 control-label">Slider New Image</label>

			                  <div class="col-sm-8">
			                    <input type="file" class="input-file uniform-on form-control" id="fileInput" name="slider_image">
			                  </div>
			                </div>
            				
            			</div>
            		</div>
            	</div>
            	<div class="box-footer">
            		<div class="row">
            			<div class="col-md-8">
            				<button type="submit" class="btn btn-primary pull-right"> Update
                	       </button>

		                	<button type="reset" class="btn btn-default">Cancel
		                	</button>
            			</div>
            		</div>
            	</div>
            </form>
			</div>
		</div>
	</div>
</section>

@endsection
