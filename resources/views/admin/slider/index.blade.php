@extends('layouts.app')

@section('title','All Slider')

@section('content')

<section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
        <li class="active"><i class="fa fa-slideshare" aria-hidden="true"> </i> All Slider</li>
      </ol>
</section> 

<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-success">
				<div class="box-header with-border">
					<h3 class="box-title">Slider List</h3>
				</div>
               
               

              @if(session('successMsg'))
                <div class="alert alert-success alert-dismissible" role="alert">
				  <strong>Success!</strong> - {{session('successMsg')}}
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
				    <span aria-hidden="true">&times;</span>
				  </button>
				</div>
              @endif



				<div class="box-body">
					<table id="example1" class="table table-bordered table-striped table-responsive">
						
						<thead>
							<tr>
								<th>Id</th>
								<th>Image</th>
								<th>Title</th>
								<th>Subtitle</th>
								<th>Status</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							<tr>
						@foreach($sliders as $key=>$slider)		
								<td>{{$key+1}}</td>
								<td><img src="{{asset('upload/slider/'.$slider->slider_image)}}" style="width:100px;height:70px;"></td>
								<td>{{$slider->image_title}}</td>
								<td>{{$slider->image_subtitle}}</td>
								
								<td>
									@if($slider->publication_status==1)
									   <span class="label label-success">Active</span>
									@else 
									   <span class="label label-danger">Unctive</span>
									@endif
								</td>
								<td>
									@if($slider->publication_status==1)
									  <a href="{{URL::to('/unactive_slider/'.$slider->id)}}" class="btn btn-danger btn-sm"><i class="fa fa-thumbs-down" aria-hidden="true"></i></a>
									@else 
									  <a href="{{URL::to('/active_slider/'.$slider->id)}}" class="btn btn-success btn-sm"><i class="fa fa-thumbs-up" aria-hidden="true"></i></a>
									@endif

									<a href="{{route('slider.edit',$slider->id)}}" class="btn btn-info btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                     
                                    <form id="delete-form-{{$slider->id}}" action="{{route('slider.destroy',$slider->id)}}" method="POST" style="display:none">
                                    @csrf
                                    @method('DELETE')	
                                    </form>

									<button class="btn btn-danger btn-sm" onclick="if(confirm('Are you sure you want to delete this slider permanently !!')){
										event.preventDefault();
										document.getElementById('delete-form-{{$slider->id}}').submit();
									}else{
										event.preventDefault();
									}"><i class="fa fa-trash" aria-hidden="true"></i></button>
								</td>
							
							</tr>
							@endforeach	
						</tbody>
					</table>
				</div>

			</div>
		</div>
	</div>
	
</section>

@endsection

@push('scripts')

<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>

@endpush
