@extends('layouts.app')

@section('title','Add Staff')

@section('content')

<section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
        <li class="active"><i class="fa fa-user-circle-o" aria-hidden="true"></i> All Staff</li>
      </ol>
</section> 

<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-success">
				<div class="box-header with-border">
	              <h3 class="box-title">Add Staff</h3>
	              
	              <div class="box-tools pull-right">
	                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
	                </button>
	               
	                <button type="button" class="btn btn-box-tool" data-toggle="dropdown">
	                    <i class="fa fa-wrench"></i></button>
	                              
	                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
	              </div>
	            </div>
            <!-- /.box-header -->
            <form class="form-horizontal" action="{{route('staff.update',$staff->id)}}" method="POST" enctype="multipart/form-data">
            	@csrf
            	@method('put')
            	<div class="box-body">
            		<div class="row">
            			<div class="col-md-8">

            				<div class="form-group">
			                  <label for="Staff Name" class="col-sm-4 control-label">Name</label>

			                  <div class="col-sm-8">
			                    <input type="text" class="form-control" value="{{$staff->name}}" name="name">
			                  </div>
			                </div>

			                <div class="form-group">
			                  <label for="Staff Email" class="col-sm-4 control-label">Email</label>

			                  <div class="col-sm-8">
			                    <input type="text" class="form-control" value="{{$staff->email}}" name="email">
			                  </div>
			                </div>

			                <div class="form-group hidden-phone">
			                  <label for="Address" class="col-sm-4 control-label">Address</label>

			                  <div class="col-sm-8">
			                      <textarea class="textarea" name="address" 
		                      style="width: 100%; height: 60px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{$staff->address}}
		                      	
		                         </textarea>
			                  </div>
			                </div>
			                <div class="form-group">
			                  <label for="Delivary Area" class="col-sm-4 control-label">Delivary Area</label>

			                  <div class="col-sm-8">
			                    <input type="text" class="form-control" value="{{$staff->area}}" name="area">
			                  </div>
			                </div>
			                <div class="form-group">
			                  <label for="Staff Mobile" class="col-sm-4 control-label">Mobile Number</label>

			                  <div class="col-sm-8">
			                    <input type="text" class="form-control" value="{{$staff->mobile_number}}" name="mobile_number">
			                  </div>
			                </div>

			                <div class="form-group">
			                	<label for="Image" class="col-sm-4 control-label">Old Image</label>
			                   <div class="col-sm-8">
			                      <img src="{{asset('upload/staff/'.$staff->image)}}" style="width:100px;height:70px;">
			                  </div>
			                </div>

			                <div class="form-group">
			                  <label for="Image" class="col-sm-4 control-label">New Image</label>

			                  <div class="col-sm-8">
			                    <input type="file" class="input-file uniform-on form-control" id="fileInput" name="image">
			                  </div>
			                </div>
           				
            			</div>
            		</div>
            	</div>
            	<div class="box-footer">
            		<div class="row">
            			<div class="col-md-8">
            				<button type="submit" class="btn btn-primary pull-right">  Submit
                	       </button>

		                	<button type="reset" class="btn btn-default">Cancel
		                	</button>
            			</div>
            		</div>
            	</div>
            </form>
			</div>
		</div>
	</div>
</section>

@endsection