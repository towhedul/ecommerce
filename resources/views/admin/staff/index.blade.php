@extends('layouts.app')
@section('title','All Staff')

@section('content')

<section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
        <li class="active"><i class="fa fa-user-circle-o" aria-hidden="true"> </i> All Staff</li>
      </ol>
</section> 

<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-success">
				<div class="box-header with-border">
					<h3 class="box-title">Slider List</h3>
				</div>              

              @if(session('successMsg'))
                <div class="alert alert-success alert-dismissible" role="alert">
				  <strong>Success!</strong> - {{session('successMsg')}}
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
				    <span aria-hidden="true">&times;</span>
				  </button>
				</div>
              @endif


				<div class="box-body">
					<table id="example1" class="table table-bordered table-striped table-responsive">
						
						<thead>
							<tr>
								<th>Name</th>
								<th>Image</th>
								<th>Email</th>
								<th>Address</th>
								<th>Area</th>
								<th>Mobile Number</th>
								<th>Status</th>
								<th>Action</th>
							</tr>
						</thead>

						<tbody>
							<tr>
						@foreach($staffs as $key=>$staff)		
								<td>{{$staff->name}}</td>
								<td><img src="{{asset('upload/staff/'.$staff->image)}}" style="width:100px;height:70px;"></td>
								<td>{{$staff->email}}</td>
								<td>{{$staff->address}}</td>
								<td>{{$staff->area}}</td>
								<td>{{$staff->mobile_number}}</td>
								
								<td>
									@if($staff->publication_status==1)
									   <span class="label label-success">Active</span>
									@else 
									   <span class="label label-danger">Unctive</span>
									@endif
								</td>
								<td>
									@if($staff->publication_status==1)
									  <a href="{{URL::to('/unactive_staff/'.$staff->id)}}" class="btn btn-danger btn-sm"><i class="fa fa-thumbs-down" aria-hidden="true"></i></a>
									@else 
									  <a href="{{URL::to('/active_staff/'.$staff->id)}}" class="btn btn-success btn-sm"><i class="fa fa-thumbs-up" aria-hidden="true"></i></a>
									@endif

									<a href="{{route('staff.edit',$staff->id)}}" class="btn btn-info btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                     
                                    <form id="delete-form-{{$staff->id}}" action="{{route('staff.destroy',$staff->id)}}" method="POST" style="display:none">
                                    @csrf
                                    @method('DELETE')	
                                    </form>

									<button class="btn btn-danger btn-sm" onclick="if(confirm('Are you sure you want to delete this slider permanently !!')){
										event.preventDefault();
										document.getElementById('delete-form-{{$staff->id}}').submit();
									}else{
										event.preventDefault();
									}"><i class="fa fa-trash" aria-hidden="true"></i></button>
								</td>
							
							</tr>
							@endforeach	
						</tbody>
						
					</table>
				</div>

			</div>
		</div>
	</div>
	
</section>

@endsection

@push('scripts')

<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>

@endpush