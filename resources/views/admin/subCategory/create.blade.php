@extends('layouts.app')

@section('title','Add Sub Category')

@section('content')

<section class="content-header">
      <h1>
        Dashboard
        
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
        <li class="active"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Add New Sub Category</li>
      </ol>
</section> 

<section class="content">
<div class="row">
        <div class="col-md-12">
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">Add New Sub Category</h3>
              @if($errors->any())
                  @foreach($errors->all() as $error)
                     <div class="alert alert-danger alert-dismissible" role="alert">
                      <strong>Danger!</strong> - {{$error}}
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                  @endforeach
              @endif

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
               
                <button type="button" class="btn btn-box-tool" data-toggle="dropdown">
                    <i class="fa fa-wrench"></i></button>
                              
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
         <form class="form-horizontal" action="{{route('subCategory.store')}}" method="POST">
         	@csrf

            <div class="box-body">
              <div class="row">
                 <div class="col-md-8">


                 	    <div class="form-group">
		                  <label for="Subcategory Name" class="col-sm-4 control-label"> Category Name</label>

		                  <div class="col-sm-8">
                        
		                   <select class="form-control" id="sel1" name="category_id">
								<option>---Select Menu---</option>
						@foreach($categories as $key=>$category)		
								<option value="{{$category->category_id}}">{{$category->category_name}}</option>
						@endforeach		
							</select>
                        
		                  </div>
		                </div>
                 		
                 		<div class="form-group">
		                  <label for="sub_category_name" class="col-sm-4 control-label">Sub Category Name</label>

		                  <div class="col-sm-8">
		                    <input type="text" class="form-control" id="category_name" placeholder="Enter Sub Category Name" name="sub_category_name">
		                  </div>
		                </div>

		          
		                <div class="form-group hidden-phone">
		                  <label for="category_description" class="col-sm-4 control-label">Sub Category Description</label>

		                  <div class="col-sm-8">
		                      <textarea class="textarea" name="sub_category_description" placeholder="Place some text here"
                          style="width: 100%; height: 140px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">
                          	
                             </textarea>
		                  </div>
		                </div>

		                <div class="form-group">
		                  <label for="publication_status" class="col-sm-4 control-label">Publication Status</label>

		                  <div class="col-sm-8">
		                    <input type="checkbox" name="publication_status" value="1">
		                  </div>
		                </div>
                 		
                 	
                 </div>
               
             </div>
              <!-- /.row -->
            </div>
            <!-- ./box-body -->
            <div class="box-footer">
              <div class="row">
                <div class="col-md-8">
                	<button type="submit" class="btn btn-primary pull-right">Submit
                	</button>

                	<button type="reset" class="btn btn-default"> Cancel
                	</button>
                	
                </div>
                            
              </div>
              <!-- /.row -->
            </div>
        </form> 
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>

  </section> 


@endsection