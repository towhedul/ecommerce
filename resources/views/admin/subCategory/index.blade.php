@extends('layouts.app')

@section('title','All Subcategory')

@section('content')

<section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
        <li class="active"><i class="fa fa-list"> </i> All Subcategory</li>
      </ol>
</section> 

<section class="content">
	<div class="row">
		<div class="col-xs-12">

			<div class="box box-success">
				<div class="box-header">
					<h3 class="box-title">Subcategory List</h3>
				</div>

				@if(session('successMsg'))

		         <div class="alert alert-success alert-dismissible" role="alert">		    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		          </button>
		            <span>Success - {{session('successMsg')}}</span>
		         </div> 
		        @endif

				<div class="box-body">
					<table id="example2" class="table table-bordered table-hover table-responsive">
						<thead>
						  <tr>
						    <th>Subcategory Id</th>
						    <th>Subcategory Name</th>
						    <th>Category Name</th>
						    <th>Subcategory Description</th>
						    <th>Status</th>
						    <th>Action</th>
						  </tr>
						</thead>
						<tbody>
					@foreach($sub_categories as $key=>$sub_category) 		
						  <tr>
						 	
						    <td>{{$sub_category->sub_category_id}}</td>
						    <td>{{$sub_category->sub_category_name}}</td>
						    <td>{{$sub_category->category_name}}</td>
						    <td>{{$sub_category->sub_category_description}}</td>
						    <td>
						    	@if($sub_category->publication_status==1)
						    	<span class="label label-success">Active</span>
						    	@else
                                <span class="label label-danger">Unactive</span>
                                @endif
						    </td>
						    <td>
                                @if($sub_category->publication_status==1)

						    	<a href="{{URL::to('/unactive_subcategory/'.$sub_category->sub_category_id)}}" class="btn btn-danger btn-sm"><i class="fa fa-thumbs-down" aria-hidden="true"></i></a>
						    	@else
						    	<a href="{{URL::to('/active_subcategory/'.$sub_category->sub_category_id)}}" class="btn btn-success btn-sm"><i class="fa fa-thumbs-up" aria-hidden="true"></i></a>
						    	@endif

						    	<a href="{{route('subCategory.edit',$sub_category->sub_category_id)}}" class="btn btn-info btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>

						    	<form id="delete-form-{{$sub_category->sub_category_id}}"action="{{route('subCategory.destroy',$sub_category->sub_category_id)}}" method="POST" style="display:none">
						    		@csrf
						    		@method('DELETE')
						    		
						    	</form>
						    	<button class="btn btn-danger btn-sm" onclick="if(confirm('! Are You Sure You Want to Delete This Record ?')){
						    		event.preventDefault();
						    		document.getElementById('delete-form-{{$sub_category->sub_category_id}}'
						    			).submit();
						    	}else{

						    		event.preventDefault();
						    	}

						    	"><i class="fa fa-trash" aria-hidden="true"></i></button>
						    </td>
						
						  </tr>
					@endforeach
						</tbody>
					</table>
				</div>
			</div>
			
		</div>
	</div>
</section>




@endsection

@push('scripts')

<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>

@endpush