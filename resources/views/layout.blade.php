<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>@yield('title')</title>
    <link href="{{asset('frontend/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('frontend/css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('frontend/css/prettyPhoto.css')}}" rel="stylesheet">
    <link href="{{asset('frontend/css/price-range.css')}}" rel="stylesheet">
    <link href="{{asset('frontend/css/animate.css')}}" rel="stylesheet">
    <link href="{{asset('frontend/css/main.css')}}" rel="stylesheet">
    <link href="{{asset('frontend/css/responsive.css')}}" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="{{URL::to('frontend/images/ico/favicon.ico')}}">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{URL::to('frontend/images/ico/apple-touch-icon-144-precomposed.png')}}">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{URL::to('frontend/images/ico/apple-touch-icon-114-precomposed.png')}}">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{URL::to('frontend/images/ico/apple-touch-icon-72-precomposed.png')}}">
    <link rel="apple-touch-icon-precomposed" href="{{URL::to('frontend/images/ico/apple-touch-icon-57-precomposed.png')}}">
    @stack('css')
</head><!--/head-->

<body>
    <header id="header"><!--header-->
        <div class="header_top"><!--header_top-->
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="contactinfo">
                            <ul class="nav nav-pills">
                                <li><a href="#"><i class="fa fa-phone"></i> +1862827975</a></li>
                                <li><a href="#"><i class="fa fa-envelope"></i> info@eshoper.com</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="social-icons pull-right">
                            <ul class="nav navbar-nav">
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div><!--/header_top-->
        
        <div class="header-middle"><!--header-middle-->
            <div class="container">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="logo pull-left">
                            <a href="index.html"><img src="{{URL::to('frontend/images/home/logo.png')}}" alt="" /></a>
                        </div>
                        <!-- <div class="btn-group pull-right">
                            <div class="btn-group">
                                <button type="button" class="btn btn-default dropdown-toggle usa" data-toggle="dropdown">
                                    USA
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu">
                                    <li><a href="#">Canada</a></li>
                                    <li><a href="#">UK</a></li>
                                </ul>
                            </div>
                            
                            <div class="btn-group">
                                <button type="button" class="btn btn-default dropdown-toggle usa" data-toggle="dropdown">
                                    DOLLAR
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu">
                                    <li><a href="#">Canadian Dollar</a></li>
                                    <li><a href="#">Pound</a></li>
                                </ul>
                            </div>
                        </div> -->
                    </div>
                    <div class="col-sm-8">
                        <div class="shop-menu pull-right">
                            <ul class="nav navbar-nav">
                                <li><a href="#"><i class="fa fa-user"></i> Account</a></li>
                                <li><a href="#"><i class="fa fa-star"></i> Wishlist</a></li>
                        <?php 
                            $customer_id=Session::get('customer_id');

                            /*echo $customer_id;*/
                        ?>
                            @if(!empty ($customer_id))
                                <li><a href="{{URL::to('/checkout')}}"><i class="fa fa-crosshairs"></i> Checkout</a></li>

                            @else
                                <li><a href="{{URL::to('/login_check')}}"><i class="fa fa-crosshairs"></i> Checkout</a></li>

                            @endif
                                
                                <li><a href="{{URL::to('/show_cart')}}"><i class="fa fa-shopping-cart"></i> Cart</a></li>
                        <?php 
                            $customer_id=Session::get('customer_id');

                            /*echo $customer_id;*/
                        ?>
                            @if(!empty($customer_id))
                                <li><a href="{{URL::to('/user_logout')}}"><i class="fa fa-lock"></i> Logout</a></li>
                            @else 

                            <li><a href="{{URL::to('/login_check')}}"><i class="fa fa-lock"></i> Login</a></li>

                            @endif
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div><!--/header-middle-->
    
        <div class="header-bottom"><!--header-bottom-->
            <div class="container">
                <div class="row">
                    <div class="col-sm-9">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        <div class="mainmenu pull-left">
                            <ul class="nav navbar-nav collapse navbar-collapse">
                                <li><a href="{{URL::to('/')}}" class="active">Home</a></li>
                                <li class="dropdown"><a href="#">Shop<i class="fa fa-angle-down"></i></a>
                                    <ul role="menu" class="sub-menu">
                                        <li><a href="{{URL::to('/')}}">Products</a></li>
                                       <!--  <li><a href="product-details.html">Product Details</a></li> -->
                            <?php $custmer_id = Session::get('customer_id'); ?>
                                    @if(!empty($custmer_id)) 
                                       <li><a href="{{URL::to('/checkout')}}">Checkout</a></li> 
                                    @else
                                       <li><a href="{{URL::to('/login_check')}}">Checkout</a></li> 
                                    @endif
                                        
                                        <li><a href="{{URL::to('/show_cart')}}">Cart</a></li>
                              <?php $customer_id = Session::get('customer_id');?> 
                                        @if(!empty($customer_id))
                                            
                                        <li><a href="{{URL::to('/user_logout')}}">Logout</a></li> 
                                        @else 
                                        <li><a href="{{URL::to('/login_check')}}">Login</a></li> 
                                        @endif
                                    </ul>
                                </li> 
                                <li class="dropdown"><a href="#">Blog<i class="fa fa-angle-down"></i></a>
                                    <ul role="menu" class="sub-menu">
                                        <li><a href="{{URL::to('/blog')}}">Blog List</a></li>
                                        <li><a href="">Blog Single</a></li>
                                    </ul>
                                </li> 
                                <!-- <li><a href="404.html">404</a></li> -->
                                <li><a href="{{URL::to('/show_contact')}}">Contact</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <form action="" method="get">
                                                    
                        <div class="search_box pull-right">
                            <input type="text" name="search" placeholder="Search"/>
                            <button type="button" class="search_button" style=" background: #F0F0E9; border: 0px; height: 33px; width:35px"><i class="fa fa-search" aria-hidden="true"></i></button>
                        </div>

                        </form>
                    </div>
                </div>
            </div>
        </div><!--/header-bottom-->
    </header><!--/header-->
    
    <!--Start Slider Section-->
  @if(Request::is('/'))
    @include('Pages.partial.slider');
  @endif

    <!--End Slider Section-->
    
    <section>
        <div class="container">
            <div class="row">
                <div class="col-sm-3">
                    <div class="left-sidebar">
                        <h2>Category</h2>
                        <div class="panel-group category-products" id="accordian"><!--category-productsr--> 
                            
                        <?php 

                         $categories=DB::table('tbl_category')
                                    ->where('publication_status',1)
                                    ->get();

                        ?>

                        <?php 

                         $sub_categories=DB::table('tbl_sub_category')
                                    ->where('publication_status',1)
                                    ->get();

                        ?> 

             
                        @foreach($categories as $key=>$category)
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordian" href="#{{$category->category_id}}">
                                            <span class="badge pull-right"><i class="fa fa-plus"></i></span>
                                            {{$category->category_name}}
                                        </a>
                                    </h4>
                                </div>

                                <div id="{{$category->category_id}}" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <ul>
                      
                                   @foreach($sub_categories as $sub)
                                        @if($category->category_id == $sub->category_id)
                                     
                                            <li><a href="{{URL::to('/product_by_subcategory/'.$sub->sub_category_id)}}">{{$sub->sub_category_name}}</a></li>
                                        @endif


                                     @endforeach 
                                        
                                        </ul>
                                    </div>
                                </div>


                            </div>
                        @endforeach

                        


                        </div><!--/category-products-->

                     <?php 
                     
                             $brands=DB::table('tbl_brand')
                                     ->where('publication_status',1)
                                     ->get();

                     ?>
                     
                    
                        <div class="brands_products"><!--brands_products-->
                            <h2>Brands</h2>
                            <div class="brands-name">
                                <ul class="nav nav-pills nav-stacked">
                                @foreach($brands as $brand)    
                                    <li><a href="{{URL::to('/product_by_brand/'.$brand->brand_name)}}"> <span class="pull-right">(30)</span>{{$brand->brand_name}}</a></li>
                                @endforeach    
                                </ul>
                            </div>
                        </div><!--/brands_products-->


                        
                        <div class="price-range"><!--price-range-->
                            <h2>Price Range</h2>
                            <div class="well text-center">
                                 <input type="text" class="span2" value="" data-slider-min="0" data-slider-max="600" data-slider-step="5" data-slider-value="[250,450]" id="sl2" ><br />
                                 <b class="pull-left">$ 0</b> <b class="pull-right">$ 600</b>
                            </div>
                        </div><!--/price-range-->
                        
                        <div class="shipping text-center"><!--shipping-->
                            <img src="{{URL::to('frontend/images/home/shipping.jpg')}}" alt="" />
                        </div><!--/shipping-->
                    
                    </div>
                </div>

             @yield('content')  

             
                
            </div>
        </div>
    </section>
    
        <!--Start Footer Section-->
                  
        @include('Pages.partial.footer')
        
    
    

  
    <script src="{{asset('frontend/js/jquery.js')}}"></script>
    <script src="{{asset('frontend/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('frontend/js/jquery.scrollUp.min.js')}}"></script>
    <script src="{{asset('frontend/js/price-range.js')}}"></script>
    <script src="{{asset('frontend/js/jquery.prettyPhoto.js')}}"></script>
    <script src="{{asset('frontend/js/main.js')}}"></script>

@stack('scripts')
</body>
</html>



