 <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{{asset('backend/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>{{Session::get('admin_name')}}</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat">
                  <i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="{{Request::is('admin/dashboard*') ? 'active': ''}}">
          <a href="{{url('admin/dashboard')}}">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          
        </li>
        
        
        
        <li class="treeview {{Request::is('admin/category*') ? 'active': ''}}">
          <a href="">
            <i class="fa fa-laptop"></i>
            <span>Category</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{url('admin/category')}}"><i class="fa fa-circle-o"></i> All Category</a></li>
            <li><a href="{{url('admin/category/create')}}"><i class="fa fa-circle-o"></i> Add Category</a></li>
            <li><a href=""><i class="fa fa-circle-o"></i> Buttons</a></li>
            
          </ul>
        </li>
        <li class="treeview {{Request::is('admin/subCategory*') ? 'active':''}}">
          <a href="#">
            <i class="fa fa-edit"></i> <span>Sub Category</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{url('admin/subCategory')}}"><i class="fa fa-circle-o"></i>All Sub Category</a></li>
            <li><a href="{{url('admin/subCategory/create')}}"><i class="fa fa-circle-o"></i> Add Sub Category</a></li>
            
          </ul>
        </li>

        <li class="treeview {{Request::is('admin/products*') ? 'active':''}}">
          <a href="#">
            <i class="fa fa-list"></i> <span>Products</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{url('admin/products')}}"><i class="fa fa-circle-o"></i>All Products</a></li>
            <li><a href="{{url('admin/products/create')}}"><i class="fa fa-circle-o"></i> Add Products</a></li>
            
          </ul>
        </li>

        <li class="treeview {{Request::is('admin/slider*') ? 'active':''}}">
          <a href="#">
            <i class="fa fa-sliders"></i> <span>Sliders</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{url('admin/slider')}}"><i class="fa fa-circle-o"></i>All Sliders</a></li>
            <li><a href="{{url('admin/slider/create')}}"><i class="fa fa-circle-o"></i> Add Sliders</a></li>
            
          </ul>
        </li>



        <li class="treeview {{Request::is('admin/brand*') ? 'active' : ''}}">
          <a href="#">
            <i class="fa fa-table"></i> <span>Brand</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{url('admin/brand')}}"><i class="fa fa-circle-o"></i> All Brand</a></li>
            <li><a href="{{url('admin/brand/create')}}"><i class="fa fa-circle-o"></i> Add Brand</a></li>
          </ul>
        </li>

        <li class="treeview {{Request::is('admin/blog*') ? 'active' : ''}}">
          <a href="#">
            <i class="fa fa-address-card" aria-hidden="true"></i> <span>Blog</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{url('admin/blog')}}"><i class="fa fa-circle-o"></i> All Blog</a></li>
            <li><a href="{{url('admin/blog/create')}}"><i class="fa fa-circle-o"></i> Add Blog</a></li>
          </ul>
        </li>

        <li class="treeview {{Request::is('admin/staff*') ? 'active' : ''}}">
          <a href="#">
            <i class="fa fa-user-plus" aria-hidden="true"></i> <span>Staff</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{url('admin/staff')}}"><i class="fa fa-circle-o"></i> Delivary Staff</a></li>
            <li><a href="{{url('admin/staff/create')}}"><i class="fa fa-circle-o"></i> Add Delivary Staff</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-table"></i> <span>Tables</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <!-- <ul class="treeview-menu">
            <li><a href=""><i class="fa fa-circle-o"></i> Simple tables</a></li>
            <li><a href=""><i class="fa fa-circle-o"></i> Data tables</a></li>
          </ul> -->
        </li>

        <li class="{{Request::is('admin/manage_order') ? 'active' : ''}}"><a href="{{url('admin/manage_order')}}"><i class="fa fa-first-order" aria-hidden="true"></i> <span>Manage Order</span></a></li>

        <li class="{{Request::is('admin/manage_payment') ? 'active' : ''}}"><a href="{{url('admin/manage_payment')}}"><i class="fa fa-credit-card-alt" aria-hidden="true"></i></i> <span>Manage Payment</span></a></li>

        <li class="{{Request::is('admin/show') ? 'active' : ''}}"><a href="{{url('admin/show')}}"><i class="fa fa-book"></i> <span>Documentation</span></a></li>

      </ul>
    </section>