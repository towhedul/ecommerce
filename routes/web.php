<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//--------------Frontend Pages----------------------


Route::get('/','HomeController@index');
Route::get('/product_by_subcategory/{sub_category_id}','HomeController@product_by_subcategory');

Route::get('/product_by_brand/{brand_name}','HomeController@product_by_brand');
Route::get('/product_details/{product_id}','HomeController@product_details');

//----------------Cart Controller------------------------
Route::post('/add_to_cart','CartController@add_to_cart');
Route::get('/show_cart','CartController@show_cart');
Route::get('/delete_to_cart/{id}','CartController@delete_to_cart');
Route::get('/increment_quantity/{id}','CartController@increment_quantity');
Route::get('/decrement_quantity/{id}','CartController@decrement_quantity');



//-------------------Payment by Authorized.Net---------------------------
Route::get('/payment_by_authorize','PaymentByAuthorizeController@index');
Route::post('/payment_complete','PaymentByAuthorizeController@chargeCreditCard');


//-------------------Payment by paypal-------------------------------------
Route::get('/payment_by_paypal','PaymentByPaypalController@index');
Route::post('/paypal_payment','PaymentByPaypalController@paypal');
Route::get('processPaypal','PaymentByPaypalController@returnPaypal')->name('process.paypal');
Route::get('cancelPaypal','PaymentByPaypalController@cancelPaypal')->name('cancel.paypal');




//-----------------Checkout Controller-----------------

Route::get('/login_check','CheckoutController@login_check');
Route::post('/user_registration','CheckoutController@user_registration');
Route::get('/checkout','CheckoutController@checkout');
Route::post('/shipping_details','CheckoutController@shipping_details');
Route::post('/checkout','CheckoutController@store');


//---------------------user Login and Use Logout-----------------
Route::post('/user_login','CheckoutController@user_login');
Route::get('/user_logout','CheckoutController@user_logout');
Route::get('/payment','CheckoutController@payment');
Route::get('/payment_success','CheckoutController@payment_success');
Route::get('/success_done','CheckoutController@success_done');


//--------------------Some Extra Pages Controller----------------------

Route::get('/show_contact','ExtraPagesController@show_contact');
Route::get('/blog','ExtraPagesController@blog_show');

//------------Backend Pages---------------------------

Route::get('/admin','AdminController@index');
Route::get('/dashboard','SuperAdminController@index');
Route::post('/admin/dashboard','AdminController@dashboard');
Route::get('/show_order_details/{order_id}','AdminController@show_order_details');
Route::get('/admin/invoice/{order_id}','AdminController@invoice');

Route::get('/logout','SuperAdminController@logout');


Route::get('/unactive/{category_id}','StatusController@unactive');
Route::get('/active/{category_id}','StatusController@active');
Route::get('/active_subcategory/{sub_category_id}','StatusController@active_subcategory');
Route::get('/unactive_subcategory/{sub_category_id}','StatusController@unactive_subcategory');

Route::get('/active_product/{product_id}','StatusController@active_product');
Route::get('/unactive_product/{product_id}','StatusController@unactive_product');

Route::get('/unactive_slider/{id}','StatusController@active_slider');
Route::get('/active_slider/{id}','StatusController@unactive_slider');

Route::get('/unactive_brand/{brand_id}','StatusController@active_brand');
Route::get('/active_brand/{brand_id}','StatusController@unactive_brand');

Route::get('/unactive_staff/{id}','StatusController@active_staff');
Route::get('/active_staff/{id}','StatusController@unactive_staff');




Route::get('/show_orderDetails_id/{order_id}','CartController@show_orderDetails_id');

Route::group(['prefix'=>'admin','namespace'=>'admin'],function(){

    
     Route::get('/dashboard','AdminController@index')->name('admin.dashboard');
     Route::resource('category','CategoryController');
     Route::resource('subCategory','SubCategoryController');
     Route::resource('products','ProductController');
     Route::resource('slider','SliderController');
     Route::resource('brand','BrandController');
     Route::resource('blog','BlogController');
     Route::resource('staff','StaffController');
     Route::resource('manage_order','ManageOrderController');
     Route::resource('manage_payment','ManagePaymentController');

    


});